package command;

import dao.FacultyDao;
import model.Faculty;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.AssertTrue;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
class CreateFacultyCommandTest {
@InjectMocks
    CreateFacultyCommand createFacultyCommand;
    @Mock
    FacultyDao facultyDao;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    int id = 5;

    @BeforeEach
    void setUp() {
        createFacultyCommand = new CreateFacultyCommand();
        request = mock(HttpServletRequest.class);
        facultyDao = mock(FacultyDao.class);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void execute() {
        when(request.getParameter("englishName")).thenReturn("englishName");
        when(request.getParameter("ukrainianName")).thenReturn("englishName");
        when(request.getParameter("russianName")).thenReturn("englishName");
        when(request.getParameter("budgetQty")).thenReturn("englishName");
        when(request.getParameter("totalQty")).thenReturn("englishName");

        Faculty faculty = new Faculty();
        when(facultyDao.create(any(), any())).thenReturn(5);
        createFacultyCommand.execute(request,response);

        assertEquals("englishName", request.getParameter("englishName"));
        verify(facultyDao, times(1)).create(faculty, new String[]{});
    }
}
