package dao;

import db.DBManager;
import db.Fields;
import model.Grade;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import javax.activation.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class GradeDaoTest {
    @Mock
    DBManager mockDbManager;
    @Mock
    Connection mockConn;
    @Mock
    PreparedStatement mockPreparedStmnt;
    @Mock
    ResultSet mockResultSet;

    int userId = 5;

    @BeforeEach
    void setUp() throws SQLException {
        when(DBManager.getInstance().getConnection()).thenReturn(mockConn);
        doNothing().when(mockConn).commit();
        when(mockConn.prepareStatement(anyString(), anyInt())).thenReturn(mockPreparedStmnt);
        doNothing().when(mockPreparedStmnt).setInt(anyInt(), anyInt());
        when(mockPreparedStmnt.executeUpdate()).thenReturn(anyInt());
        when(mockPreparedStmnt.getGeneratedKeys()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(1)).thenReturn(userId);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getById() {
    }

    @Test
    void getAll() {
    }

    @Test
    void create() throws SQLException {
        GradeDao gradeDao = new GradeDao();
        gradeDao.create(new Grade(), new String[]{});
        verify(mockConn, times(1)).prepareStatement(anyString(), anyInt());
    }

    @Test
    void delete() {
    }
}