package dao;

import java.util.List;

public interface Dao<T> {
    T getById(int id, String[] params);
    List<T> getAll(String[] params);
    int create(T t, String[] params);
    boolean update(T t, String[] params);
    boolean delete(int id);
}
