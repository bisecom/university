package dao;

import db.DBManager;
import db.Fields;
import model.Application;
import model.Grade;
import model.Subject;
import org.apache.log4j.Logger;
import utils.EntityMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
/**
 * Fulfill crud operations for Grade entity with the db
 */
public class GradeDao implements Dao<Grade>{
    private static final Logger log = Logger.getLogger(GradeDao.class);

    private static final String SQL__INSERT_GRADE =
            "INSERT INTO grades(subjects_id, grade)VALUES(?, ?);";
    private static final String SQL__INSERT_SET_APPL_GRADE =
            "INSERT INTO applications_grades(applications_id, grades_id)VALUES(?,?);";
    private static final String SQL__GET_GRADE_BY_ID =
            "SELECT gr.id, su.id as subj_id, sl.name, su.duration, gr.grade\n" +
                    "FROM grades gr\n" +
                    "INNER JOIN subjects su ON su.id = gr.subjects_id\n" +
                    "INNER JOIN subjects_languages sl ON sl.subjects_id = su.id\n" +
                    "INNER JOIN languages la ON la.id = sl.languages_id\n" +
                    "WHERE gr.id = ? AND la.lang_code = ?;";
    private static final String SQL__GET_ALL_GRADES_BY_APPL_ID =
            "SELECT gr.id, su.id as subj_id, sl.name, su.duration, gr.grade\n" +
                    "FROM grades gr\n" +
                    "INNER JOIN applications_grades ag ON ag.grades_id = gr.id\n" +
                    "INNER JOIN subjects su ON su.id = gr.subjects_id\n" +
                    "INNER JOIN subjects_languages sl ON sl.subjects_id = su.id\n" +
                    "INNER JOIN languages la ON la.id = sl.languages_id\n" +
                    "WHERE ag.applications_id = ? AND la.lang_code = ?;";
    private static final String SQL__DELETE_GRADE_BY_ID =
            "DELETE FROM grades WHERE id = ?;";


    @Override
    public Grade getById(int id, String[] params) {
        log.debug("getById grade --> started");
        Grade grade = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            GradeMapper mapper = new GradeMapper();
            pstmt = con.prepareStatement(SQL__GET_GRADE_BY_ID);
            pstmt.setInt(1, id);
            pstmt.setString(2, params[0]);
            rs = pstmt.executeQuery();
            if (rs.next()){
                grade = mapper.mapRow(rs);
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("getById grade --> finished");
        return grade;
    }

    /**
     * Get list of grades for indicated application id and locale
     * @param params application id and locale
     * @return list of grades
     */
    @Override
    public List<Grade> getAll(String[] params) {
        log.debug("getAll grades --> started");
        List<Grade> gradesList = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        if (params == null || params.length == 0)
            return gradesList;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__GET_ALL_GRADES_BY_APPL_ID);
            pstmt.setInt(1, Integer.parseInt(params[1]));
            pstmt.setString(2, params[0]);
            rs = pstmt.executeQuery();
            GradeMapper mapper = new GradeMapper();
            while (rs.next())
                gradesList.add(mapper.mapRow(rs));
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("getAll grades size() --> " + gradesList.size());
        return gradesList;
    }

    /**
     * Insert grade entity to the db
     * @param grade entity to insert
     * @param params of locale
     * @return row id
     */
    @Override
    public int create(Grade grade, String[] params) {
        PreparedStatement pstmt;
        Connection con = null;
        int createdId = 0;
        log.debug("create before connection grade -->" + grade.getSubject().getNameList().get(0));
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__INSERT_GRADE, Statement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, grade.getSubject().getId());
            pstmt.setInt(2, grade.getGrade());
            if (pstmt.executeUpdate() == 0) {
                throw new SQLException("Creating grade failed, no rows affected.");
            }
            try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    createdId = generatedKeys.getInt(1);
                } else {
                    throw new SQLException("Creating grade failed, no ID obtained.");
                }
            }
            pstmt = con.prepareStatement(SQL__INSERT_SET_APPL_GRADE);
            //application id
            pstmt.setInt(1, Integer.parseInt(params[1]));
            // grade id
            pstmt.setInt(2, createdId);

            if (pstmt.executeUpdate() == 0) {
                throw new SQLException("Creating appl_grade set failed, no rows affected.");
            }
        } catch (SQLException throwables) {
            log.debug("Difficulties to get id --> " + throwables);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("create new grade for subject name (en) -->" + grade.getSubject().getNameList().get(0));
        return createdId;
    }

    public boolean createGradesSet(Application application, String[] params) {
        PreparedStatement pstmt;
        Connection con = null;
        int createdId = 0;
        log.debug("createGradesSet before connection grades list size -->" + application.getGradesList().size());
        try {
            con = DBManager.getInstance().getConnection();

            for (Grade g : application.getGradesList()) {
                pstmt = con.prepareStatement(SQL__INSERT_GRADE, Statement.RETURN_GENERATED_KEYS);
                pstmt.setInt(1, g.getSubject().getId());
                pstmt.setInt(2, g.getGrade());
                if (pstmt.executeUpdate() == 0) {
                    throw new SQLException("Creating grade failed, no rows affected.");
                }
                try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        createdId = generatedKeys.getInt(1);
                    } else {
                        throw new SQLException("Creating grade failed, no ID obtained.");
                    }
                }
                pstmt = con.prepareStatement(SQL__INSERT_SET_APPL_GRADE);
                //application id
                pstmt.setInt(1, Integer.parseInt(params[1]));
                // grade id
                pstmt.setInt(2, createdId);

                if (pstmt.executeUpdate() == 0) {
                    throw new SQLException("Creating appl_grade set failed, no rows affected.");
                }
            }

        } catch (SQLException throwables) {
            log.debug("Difficulties to get id --> " + throwables);
            return false;
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("createGradesSet finished -->");
        return true;
    }

    /**
     * The method is not valid for the entity
     * @param grade entity
     * @param params locale data
     * @return false always
     */
    @Override
    public boolean update(Grade grade, String[] params) {
        return false;
    }

    /**
     * Delete grade instance by id
     * @param id of grade entity
     * @return true if succeed
     */
    @Override
    public boolean delete(int id) {
        log.debug("delete grade started for id --> " + id);
        PreparedStatement pstmt;
        Connection con = null;
        if (id == 0 || id < 0)
            return false;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__DELETE_GRADE_BY_ID);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (Exception e) {
            log.trace("Grade is not deleted", e);
            return false;
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("delete grade result --> true");
        return true;
    }

    /**
     * Extracts an grade from the result set row.
     */
    private static class GradeMapper implements EntityMapper<Grade> {
        @Override
        public Grade mapRow(ResultSet rs) {
            try {
                Grade grade = new Grade();
                Subject subject = new Subject();
                grade.setId(rs.getInt(Fields.GRADE_ID));
                grade.setGrade(rs.getInt(Fields.GRADE_VALUE));

                subject.setId(rs.getInt("subj_id"));
                subject.getNameList().add(rs.getString(Fields.SUBJECT_NAME));
                subject.setCourseDuration(rs.getInt(Fields.SUBJECT_DURATION));
                grade.setSubject(subject);
                return grade;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
