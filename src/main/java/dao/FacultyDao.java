package dao;

import db.DBManager;
import db.Fields;
import model.Faculty;
import model.Subject;
import org.apache.log4j.Logger;
import utils.EntityMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Fulfill crud operations for Faculty entity with the db
 */
public class FacultyDao implements Dao<Faculty>{
    private static final Logger log = Logger.getLogger(FacultyDao.class);

    private static final String SQL__INSERT_FACULTY =
            "INSERT INTO faculties (budget_places_qty, total_places_qty) VALUES (?, ?);";

    private static final String SQL__INSERT_FACULTY_LANG =
                    "INSERT INTO faculties_languages (faculties_id, languages_id, name) \n" +
                    "VALUES \n" +
                    "((SELECT MAX(id) FROM faculties), (SELECT id FROM languages WHERE lang_code = 'en'), ?),\n" +
                    "((SELECT MAX(id) FROM faculties), (SELECT id FROM languages WHERE lang_code = 'uk'), ?),\n" +
                    "((SELECT MAX(id) FROM faculties), (SELECT id FROM languages WHERE lang_code = 'ru'), ?);";

    private static final String SQL__INSERT_SET_FACULTY_SUBJ =
            "INSERT INTO faculties_subjects (faculties_id, subjects_id) VALUES (?, ?);";

    private static final String SQL__DELETE_SET_FACULTY_SUBJ =
            "DELETE FROM faculties_subjects WHERE faculties_id = ?;";

    private static final String SQL__SELECT_FACULTY_BY_ID =
            "SELECT fa.id, fa.budget_places_qty, fa.total_places_qty, GROUP_CONCAT(fl.name SEPARATOR '; ') as name\n" +
                    "FROM faculties fa\n" +
                    "INNER JOIN faculties_languages fl ON fa.id = fl.faculties_id\n" +
                    "INNER JOIN languages la ON la.id = fl.languages_id\n" +
                    "WHERE fa.id = ?\n" +
                    "GROUP BY fa.id;";

    private static final String SQL__SELECT_ALL_FACULTIES =
            "select fa.id, fl.name, fa.budget_places_qty, fa.total_places_qty\n" +
                    "from faculties fa\n" +
                    "inner join faculties_languages fl on fa.id = fl.faculties_id\n" +
                    "inner join languages l on fl.languages_id = l.id\n" +
                    "where l.lang_code = ?;";

    private static final String SQL__UPDATE_FACULTY =
            "UPDATE faculties SET budget_places_qty = ?, total_places_qty = ?\n" +
                    "WHERE id = ?;";

    private static final String SQL__UPDATE_FACULTY_LANG_SET =
                    "UPDATE faculties_languages SET name = ?\n" +
                    "WHERE faculties_id = ? AND languages_id = ?;";

    private static final String SQL__DELETE_FACULTY_BY_ID =
            "DELETE FROM faculties WHERE id = ?;";

    /**
     * Get faculty by id
     * @param id of the faculty
     * @param params of locale
     * @return Faculty instance
     */
    @Override
    public Faculty getById(int id, String[] params) {
        log.debug("getById faculty before connection id-->" + id);
        Faculty faculty = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            FacultyMapper mapper = new FacultyMapper();
            pstmt = con.prepareStatement(SQL__SELECT_FACULTY_BY_ID);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                faculty = mapper.mapRow(rs);
            }

            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("getById finish faculty name -->" + faculty.getNamesList().get(0));
        return faculty;
    }

    /**
     * Get list of faculties from db
     * @param params of local
     * @return list of faculties
     */
    @Override
    public List<Faculty> getAll(String[] params) {
        log.debug("getAll started with params --> " + params[0]);
        List<Faculty> facultyList = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__SELECT_ALL_FACULTIES);
            pstmt.setString(1, params[0]);
            rs = pstmt.executeQuery();
            FacultyMapper mapper = new FacultyMapper();
            while (rs.next())
                facultyList.add(mapper.mapRow(rs));

            //add subjects to every faculty
            for (Faculty f : facultyList) {
                List<Subject> subjectList = new SubjectDao().getAllByFacultyId(new String[]{params[0], String.valueOf(f.getId())});
                f.getSubjectList().addAll(subjectList);
            }
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("getFaculties size() --> " + facultyList.size());
        return facultyList;
    }

    /**
     * Insert faculty into db
     * @param faculty instance to insert
     * @param params add params
     * @return id of roaw
     */
    @Override
    public int create(Faculty faculty, String[] params) {
        PreparedStatement pstmt;
        Connection con = null;
        int createdId = 0;
        log.debug("create before connection faculty -->" + faculty.getNamesList().get(0));
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__INSERT_FACULTY, Statement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, faculty.getBudgetPlacesQty());
            pstmt.setInt(2, faculty.getTotalPlacesQty());

            if (pstmt.executeUpdate() == 0) {
                throw new SQLException("Creating faculty failed, no rows affected.");
            }
            try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    createdId = generatedKeys.getInt(1);
                } else {
                    throw new SQLException("Creating faculty failed, no ID obtained.");
                }
            }

            pstmt = con.prepareStatement(SQL__INSERT_FACULTY_LANG);
            pstmt.setString(1, faculty.getNamesList().get(0));
            pstmt.setString(2, faculty.getNamesList().get(1));
            pstmt.setString(3, faculty.getNamesList().get(2));
            if (pstmt.executeUpdate() == 0) {
                throw new SQLException("Creating faculty_language failed, no rows affected.");
            }
            //insert settings among Faculty and Subjects
            insertSetFacultySubject(con, faculty);

        } catch (SQLException throwables) {
            log.debug("Difficulties to get id --> " + throwables);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("create new subject name (en) -->" + faculty.getNamesList().get(0));
        return createdId;
    }


    /**
     * Update faculty instance in the db
     * @param faculty instance to update
     * @param params locale data
     * @return true if updated
     */
    @Override
    public boolean update(Faculty faculty, String[] params) {
        if(faculty == null)
            return false;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            updateFaculty(con, faculty);
            updateFacultyLangSet(con, faculty);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return true;
    }

    /**
     * Set connection for the update
     * @param con connection instance
     * @param faculty instance to update
     * @throws SQLException exception
     */

    public void updateFaculty(Connection con, Faculty faculty) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL__UPDATE_FACULTY);
        int k = 1;
        pstmt.setInt(k++, faculty.getBudgetPlacesQty());
        pstmt.setInt(k++, faculty.getTotalPlacesQty());
        pstmt.setInt(k, faculty.getId());
        pstmt.executeUpdate();
        pstmt.close();
    }

    public void updateFacultyLangSet(Connection con, Faculty faculty) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL__UPDATE_FACULTY_LANG_SET);

        for (int i = 0; i < faculty.getNamesList().size(); i++) {
            if (i == 0) {
                pstmt.setString(1, faculty.getNamesList().get(i));
                pstmt.setInt(2, faculty.getId());
                pstmt.setInt(3, 1);
                pstmt.executeUpdate();
                log.debug("updateFacultyLangSet start name -->" + faculty.getNamesList().get(i));
            } else if (i == 1) {
                pstmt.setString(1, faculty.getNamesList().get(i));
                pstmt.setInt(2, faculty.getId());
                pstmt.setInt(3, 2);
                pstmt.executeUpdate();
                log.debug("updateFacultyLangSet start name -->" + faculty.getNamesList().get(i));
            } else if (i == 2) {
                pstmt.setString(1, faculty.getNamesList().get(i));
                pstmt.setInt(2, faculty.getId());
                pstmt.setInt(3, 3);
                pstmt.executeUpdate();
                log.debug("updateFacultyLangSet start name -->" + faculty.getNamesList().get(i));
            }
        }

        pstmt.close();
    }

    private boolean insertSetFacultySubject(Connection con, Faculty faculty) {
        PreparedStatement pstmt;
        log.debug("insertSetFacultySubject start -->" + faculty.getNamesList().get(0));
        try {
            for(Subject subject : faculty.getSubjectList()){
                pstmt = con.prepareStatement(SQL__INSERT_SET_FACULTY_SUBJ);
                pstmt.setInt(1, faculty.getId());
                pstmt.setInt(2, subject.getId());

                if (pstmt.executeUpdate() == 0) {
                    throw new SQLException("Creating faculty_subj set failed, no rows affected.");
                }
            }
        } catch (SQLException throwables) {
            log.debug("Difficulties to get id --> " + throwables);
            return false;
        }
        log.debug("insertSetFacultySubject name (en) -->" + faculty.getNamesList().get(0));
        return true;
    }

    /**
     * Delete faculty from the db
     * @param id - faculty id to delete
     * @return - true if success
     */
    @Override
    public boolean delete(int id) {
        log.debug("delete faculty started for id --> " + id);
        PreparedStatement pstmt;
        Connection con = null;
        if (id == 0 || id < 0)
            return false;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__DELETE_FACULTY_BY_ID);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (Exception e) {
            log.trace("Faculty is not deleted", e);
            return false;
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("delete faculty result --> true");
        return true;
    }

    /**
     * Extracts an faculty from the result set row.
     */
    private static class FacultyMapper implements EntityMapper<Faculty> {
        @Override
        public Faculty mapRow(ResultSet rs) {
            try {
                Faculty faculty = new Faculty();
                faculty.setId(rs.getInt(Fields.FACULTY_ID));
                faculty.getNamesList().addAll(Arrays.asList(rs.getString(Fields.FACULTY_NAME).split(Pattern.quote(";"))));
                faculty.setBudgetPlacesQty(rs.getInt(Fields.FACULTY_BUDG_PLACES));
                faculty.setTotalPlacesQty(rs.getInt(Fields.FACULTY_TOTAL_PLACES));
                return faculty;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
