package dao;

import db.DBManager;
import db.Fields;
import model.Enrollee;
import org.apache.log4j.Logger;
import utils.EntityMapper;
import utils.Role;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Fulfill crud operations for Enrollee entity with the db.
 */
public class EnrolleeDao implements Dao<Enrollee>{

    private static final Logger log = Logger.getLogger(EnrolleeDao.class);
    private static final String SQL__FIND_ENROLLEE_BY_LOGIN =
            "SELECT ul.id, ul.email, ul.password, ur.name \n" +
                    "FROM user_logins ul, user_roles ur \n" +
                    "WHERE ul.user_roles_id = ur.id AND ul.email=?;";

    private static final String SQL__FIND_ENROLLEE_BY_ID =
            "SELECT ul.id as user_logins_id, ul.email, ul.password, ul.user_roles_id, e.first_name,\n" +
                    "e.father_name, e.second_name, e.city, (SELECT name FROM states WHERE id = e.states_id) as state_name,\n" +
                    "e.school_name, e.is_blocked, e.appl_date, \n" +
                    "(SELECT school_certificate FROM certificates WHERE enrollees_user_logins_id = e.user_logins_id) as school_certificate\n" +
                    "FROM user_logins ul\n" +
                    "INNER JOIN enrollees e ON ul.id = e.user_logins_id\n" +
                    "INNER JOIN states s ON s.id = e.states_id\n" +
                    "INNER JOIN certificates c ON c.enrollees_user_logins_id = e.user_logins_id\n" +
                    "WHERE ul.id = ?;";

    private static final String SQL__GET_ALL_ENROLEES =
            "SELECT e.user_logins_id, u.email, u.password, u.user_roles_id, \n" +
                    "e.first_name, e.father_name, e.second_name, e.city, s.name, \n" +
                    "e.school_name, c.school_certificate, e.is_blocked, e.appl_date \n" +
                    "FROM enrollees e, states s, certificates c, user_logins u, applications a\n" +
                    "WHERE e.user_logins_id = u.id AND s.id = e.states_id AND a.user_logins_id = u.id\n" +
                    "AND c.enrollees_user_logins_id = e.user_logins_id AND u.user_roles_id = 2\n" +
                    "AND a.faculties_id = ? LIMIT ? OFFSET ?;";

    private static final String SQL__GET_ALL_ENROLEES_COUNT_BY_FACULTY =
            "SELECT COUNT(e.user_logins_id) \n" +
                    "FROM enrollees e, user_logins ul, applications a\n" +
                    "WHERE e.user_logins_id = ul.id AND ul.id = a.user_logins_id\n" +
                    "and a.faculties_id = ?;";

    private static final String SQL__INSERT_ENROLLEE_INIT =
            "INSERT INTO user_logins (`email`, `password`, `user_roles_id`)\n" +
                    "SELECT ?, ?, id\n" +
                    "FROM user_roles\n" +
                    "WHERE name = 'USER';";

    private static final String SQL__INSERT_CERTIFICATE =
            "INSERT INTO certificates (school_certificate, enrollees_user_logins_id) VALUES (?, ?);";

    private static final String SQL__UPDATE_CERTIFICATE =
            "UPDATE certificates SET school_certificate = ? WHERE enrollees_user_logins_id = ?;";

    private static final String SQL__GET_CERTIFICATE_BY_ENROLLEE_ID =
            "SELECT enrollees_user_logins_id as user_logins_id, school_certificate\n" +
                    "FROM certificates\n" +
                    "WHERE enrollees_user_logins_id = ?;";

    private static final String SQL__INSERT_ENROLLEE_FINAL =
            "INSERT INTO enrollees(user_logins_id, first_name, father_name, second_name, city, states_id, school_name, is_blocked, appl_date)\n" +
                    "SELECT ?, ?, ?, ?, ?, s.id, ?, ?, ?\n" +
                    "FROM states s\n" +
                    "WHERE name = ?;";

    private static final String SQL_UPDATE_ENROLLEE_BY_USER =
            "UPDATE enrollees SET first_name = ?, father_name = ?,\n" +
                    "second_name = ?, city = ?, states_id = (SELECT id FROM states WHERE name = ?),\n" +
                    "school_name = ?, appl_date = ?, is_blocked = ?\n" +
                    "WHERE user_logins_id = ?;";

    private static final String SQL_UPDATE_ENROLLEE_CERTIFICATE_BY_USER =
                    "UPDATE certificates SET school_certificate = ? WHERE enrollees_user_logins_id = ?;";

    private static final String SQL_UPDATE_ENROLLEE_BY_ADMIN =
            "UPDATE enrollees SET first_name = ?, father_name = ?, second_name = ?, is_blocked = ?\n" +
                    "WHERE user_logins_id = ?;\n";

    private static final String SQL_DELETE_ENROLLEE =
            "DELETE FROM user_logins WHERE id = ?;";

    /**
     * Insert enrollee to user_logins table
     * @param email as login of the enrollee
     * @param password enrollees pass
     * @return Enrollee entity
     */
    public Enrollee createEnrollee(String email, String password) {
        Enrollee enrollee = null;
        PreparedStatement pstmt;
        Connection con = null;
        log.debug("createEnrollee before connection login -->" + email);
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__INSERT_ENROLLEE_INIT, Statement.RETURN_GENERATED_KEYS);
            enrollee = new Enrollee();
            pstmt.setString(1, email);
            pstmt.setString(2, password);

            if (pstmt.executeUpdate() == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }

            try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    enrollee.setId(generatedKeys.getInt(1));
                    enrollee.setEmail(email);
                    enrollee.setRole(Role.USER);
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
        } catch (SQLException throwables) {
            log.debug("Difficulties to get id --> " + throwables);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("createEnrollee after insert login -->" + email);
        return enrollee;
    }

    /**
     * Returns a user with the given login.
     * @param login Enrollee login.
     * @return Enrollee entity.
     */
    public Enrollee getByLogin(String login) {
        Enrollee user = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        log.debug("getByLogin enrollee before connection login -->" + login);
        try {
            con = DBManager.getInstance().getConnection();
            log.debug("getByLogin connection --> " + con);
            user = new Enrollee();
            pstmt = con.prepareStatement(SQL__FIND_ENROLLEE_BY_LOGIN);
            pstmt.setString(1, login);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                user.setId(rs.getInt(Fields.STATE_ID));
                user.setEmail(rs.getString(Fields.USER_LOGIN));
                user.setPassword(rs.getString(Fields.USER_PASSWORD));
                user.setRole(Role.valueOf(rs.getString(Fields.USER_ROLE_NAME)));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            log.debug("findUserByLogin catch --> " + ex);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("getByLogin user login and pass --> " + user.getEmail() + " " + user.getPassword());
        return user;
    }

    /**
     * Returns a enrollee with the given identifier.
     * @param id of enrollee to get from db
     * @param params locale data
     * @return Enrollee instance
     */
    @Override
    public Enrollee getById(int id, String[] params) {
        Enrollee enrollee = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            enrollee = new Enrollee();
            pstmt = con.prepareStatement(SQL__FIND_ENROLLEE_BY_ID);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            EnrolleeMapper mapper = new EnrolleeMapper();
            if (rs.next()) {
                enrollee = mapper.mapRow(rs);
            }

            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return enrollee;
    }

    /**
     * Get all enrollees from db
     * @param params set data for select
     * @return list of enrollees
     */
    @Override
    public List<Enrollee> getAll(String[] params) {
        log.debug("getAll enrollees --> start");
        List<Enrollee> enrollesList = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__GET_ALL_ENROLEES);
            pstmt.setInt(1, Integer.parseInt(params[1]));
            pstmt.setInt(2, Integer.parseInt(params[2]));
            pstmt.setInt(3, Integer.parseInt(params[3]));
            log.debug("getAll enrollees --> parseInt(params[] " + Integer.parseInt(params[1]));
            rs = pstmt.executeQuery();
            EnrolleeMapper mapper = new EnrolleeMapper();
            while (rs.next())
                enrollesList.add(mapper.mapRow(rs));
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("getAll enrollees list size() --> " + enrollesList.size());
        return enrollesList;
    }

    public int getEnrolleesListSize(String[] params){
        log.debug("getEnrolleesListSize --> start");
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        int rowsCount = 0;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__GET_ALL_ENROLEES_COUNT_BY_FACULTY);
            pstmt.setInt(1, Integer.parseInt(params[1]));
            log.debug("getAll enrollees --> parseInt(params[0] " + params[0]);
            rs = pstmt.executeQuery();
            rs.next();
            rowsCount = rs.getInt(1);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("getEnrolleesListSize size() --> " + rowsCount);
        return rowsCount;
    }

    /**
     * Insert complete Enrollee entity to db
     * @param enrollee entity to save
     * @param params locale data
     * @return enrollee id if succeed
     */
    @Override
    public int create(Enrollee enrollee, String[] params) {
        PreparedStatement pstmt;
        Connection con = null;
        log.debug("create enrollee start for login -->" + enrollee.getEmail());
        try {
            con = DBManager.getInstance().getConnection();

            pstmt = con.prepareStatement(SQL__INSERT_ENROLLEE_FINAL);
            pstmt.setInt(1, enrollee.getId());
            pstmt.setString(2, enrollee.getFirstName());
            pstmt.setString(3, enrollee.getFatherName());
            pstmt.setString(4, enrollee.getSecondName());
            pstmt.setString(5, enrollee.getCity());
            pstmt.setString(6, enrollee.getSchoolName());
            pstmt.setInt(7, enrollee.isBlocked() ? 1 : 0);
            pstmt.setDate(8, java.sql.Date.valueOf(enrollee.getApplicationTime()));
            pstmt.setString(9, enrollee.getState());
            if (pstmt.executeUpdate() == 0) {
                throw new SQLException("Creating enrollee failed, no rows affected.");
            }
            insertCertificate(enrollee.getId(), enrollee.getSchoolCertificate(), con);

        } catch (SQLException throwables) {
            log.debug("Difficulties to insert enrollee --> " + throwables);
            return 0;
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("create enrollee finish for login -->" + enrollee.getEmail());
        return enrollee.getId();
    }

    /**
     * Certificate image insertion
     * @param enrolleeId of the enrollee
     * @param certificate byte array of the file
     * @return true if succeed
     */
    public boolean insertCertificate(int enrolleeId, byte[] certificate, Connection con) {
        PreparedStatement pstmt;
        log.debug("insertCertificate start for login -->" + enrolleeId);
        try {
            pstmt = con.prepareStatement(SQL__INSERT_CERTIFICATE);
            pstmt.setBytes(1, certificate);
            pstmt.setInt(2, enrolleeId);
            if (pstmt.executeUpdate() == 0) {
                throw new SQLException("Inserting certificate failed, no rows affected.");
            }
        } catch (SQLException throwables) {
            log.debug("Difficulties to insert certificate --> " + throwables);
            return false;
        }
        log.debug("insertCertificate finish for login -->" + enrolleeId);
        return true;
    }

    public boolean updateCertificate(int enrolleeId, byte[] certificate, String[] params) {
        log.debug("update enrollee certificate start for login -->" + enrolleeId);
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL__UPDATE_CERTIFICATE);
            int k = 1;
            pstmt.setBytes(k++, certificate);
            pstmt.setInt(k, enrolleeId);
            pstmt.executeUpdate();
            pstmt.close();

        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            log.debug("update enrollee got exception --> " + ex.getMessage());
            return false;
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("update enrollee certificate finished --> true");
        return true;
    }

    public byte[] getCertificate(int enrolleeId) {
        log.debug("getCertificate start for login -->" + enrolleeId);
        byte[] certByteArray = null;
        Connection con = null;
        PreparedStatement pstmt;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__GET_CERTIFICATE_BY_ENROLLEE_ID);
            pstmt.setInt(1, enrolleeId);
            rs = pstmt.executeQuery();
            if (rs.next()){
                Blob blob = rs.getBlob(Fields.USER_SCHOOL_CERT);
                int blobLength = (int) blob.length();
                certByteArray = blob.getBytes(1, blobLength);
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            log.debug("get certificate got exception --> " + ex.getMessage());
            return certByteArray;
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("getCertificate certificate finished --> true");
        return certByteArray;
    }

    /**
     * Updates entity by user
     * @param enrollee entity to update
     * @param params locale data
     * @return true if succeed
     */
    @Override
    public boolean update(Enrollee enrollee, String[] params) {
        log.debug("update enrollee start for login -->" + enrollee.getEmail());
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            updateEnrollee(con, enrollee, params);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            log.debug("update enrollee got exception --> " + ex.getMessage());
            return false;
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("update enrollee finished --> true");
        return true;
    }

    private void updateEnrollee(Connection con, Enrollee enrollee, String[] params) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL_UPDATE_ENROLLEE_BY_USER);
        int k = 1;
        pstmt.setString(k++, enrollee.getFirstName());
        pstmt.setString(k++, enrollee.getFatherName());
        pstmt.setString(k++, enrollee.getSecondName());
        pstmt.setString(k++, enrollee.getCity());
        pstmt.setString(k++, /*enrollee.getState()*/"Kiev");
        pstmt.setString(k++, enrollee.getSchoolName());
        pstmt.setDate(k++, java.sql.Date.valueOf(enrollee.getApplicationTime()));
        pstmt.setInt(k++, enrollee.isBlocked() ? 1 : 0);
        pstmt.setInt(k, enrollee.getId());
        pstmt.executeUpdate();

        k = 1;
        pstmt = con.prepareStatement(SQL_UPDATE_ENROLLEE_CERTIFICATE_BY_USER);
        pstmt.setBytes(k++, enrollee.getSchoolCertificate());
        pstmt.setInt(k, enrollee.getId());
        pstmt.executeUpdate();
        pstmt.close();
    }

    /**
     * Delete enrollee from db under stated id
     * @param id of the enrollee
     * @return true if succeed
     */
    @Override
    public boolean delete(int id) {
        log.debug("delete started for id --> " + id);
        PreparedStatement pstmt;
        Connection con = null;
        if (id == 0 || id < 0)
            return false;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_DELETE_ENROLLEE);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (Exception e) {
            log.trace("Subject is not deleted", e);
            return false;
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("delete result --> true");
        return true;
    }

    /**
     * Extracts an enrollee from the result set row.
     */
    private static class EnrolleeMapper implements EntityMapper<Enrollee> {

        @Override
        public Enrollee mapRow(ResultSet rs) {
            int blobLength = 0;
            byte[] blobAsBytes = null;
            try {
                Enrollee enrollee = new Enrollee();
                enrollee.setId(rs.getInt(Fields.USER_ID));
                enrollee.setEmail(rs.getString(Fields.USER_LOGIN));
                enrollee.setPassword(rs.getString(Fields.USER_PASSWORD));
                enrollee.setRole(Role.valueOf(rs.getInt(Fields.USER_ROLE) == 1 ? "ADMIN" : "USER"));
                enrollee.setFirstName(rs.getString(Fields.USER_FIRST_NAME));
                enrollee.setFatherName(rs.getString(Fields.USER_FATHER_NAME));
                enrollee.setSecondName(rs.getString(Fields.USER_SECOND_NAME));
                enrollee.setCity(rs.getString(Fields.USER_CITY));
                enrollee.setState(/*rs.getString(Fields.STATE_NAME)*/"State");
                enrollee.setSchoolName(rs.getString(Fields.USER_SCHOOL));
                Blob blob = rs.getBlob(Fields.USER_SCHOOL_CERT);
                if (blob != null) {
                    blobLength = (int) blob.length();
                    blobAsBytes = blob.getBytes(1, blobLength);
                }
                enrollee.setSchoolCertificate(blobAsBytes);
                enrollee.setBlocked(rs.getInt(Fields.USER_IS_BLOCKED) != 0);
                java.sql.Date sqlDate = rs.getDate(Fields.USER_APPL_DATE);
                enrollee.setApplicationTime(sqlDate.toLocalDate());
                return enrollee;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
