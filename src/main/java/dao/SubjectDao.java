package dao;

import db.DBManager;
import db.Fields;
import model.Subject;
import org.apache.log4j.Logger;
import utils.EntityMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
/**
 * Fulfill crud operations for Subject entity with the db
 */
public class SubjectDao implements Dao<Subject>{
    private static final Logger log = Logger.getLogger(SubjectDao.class);

    private static final String SQL__INSERT_SUBJECT =
            "INSERT INTO subjects (`duration`) VALUES (?);";

    private static final String SQL__INSERT_SUBJECT_LANG =
                    "INSERT INTO subjects_languages (`subjects_id`, `languages_id`, `name`) \n" +
                    "VALUES \n" +
                    "((SELECT MAX(id) FROM subjects), (SELECT id FROM languages WHERE lang_code = 'en'), ?),\n" +
                    "((SELECT MAX(id) FROM subjects), (SELECT id FROM languages WHERE lang_code = 'uk'), ?),\n" +
                    "((SELECT MAX(id) FROM subjects), (SELECT id FROM languages WHERE lang_code = 'ru'), ?);";

    private static final String SQL__SELECT_SUBJECTS_BY_ID =
            "SELECT su.id, su.duration, GROUP_CONCAT(sl.name SEPARATOR '; ') as name\n" +
                    "FROM subjects su\n" +
                    "INNER JOIN subjects_languages sl ON su.id = sl.subjects_id\n" +
                    "INNER JOIN languages la ON la.id = sl.languages_id\n" +
                    "WHERE su.id = ?\n" +
                    "GROUP BY su.id;";

    private static final String SQL__SELECT_ALL_SUBJECTS =
"SELECT su.id, su.duration, GROUP_CONCAT(sl.name SEPARATOR '; ') as name\n" +
        "FROM subjects su\n" +
        "INNER JOIN subjects_languages sl ON su.id = sl.subjects_id\n" +
        "INNER JOIN languages la ON la.id = sl.languages_id\n" +
        "WHERE la.lang_code = ?\n" +
        "GROUP BY su.id;";

    private static final String SQL__SELECT_SUBJECTS_BY_FACULTY_ID =
            "SELECT su.id, su.duration, sl.name\n" +
                    "FROM faculties fa, faculties_subjects fs, subjects su, subjects_languages sl, languages la\n" +
                    "WHERE fa.id = fs.faculties_id AND fs.subjects_id = su.id AND su.id = sl.subjects_id AND sl.languages_id = la.id\n" +
                    "AND la.lang_code = ? AND fa.id = ?;";

    private static final String SQL__UPDATE_SUBJECT =
            "UPDATE subjects SET `duration` = ? WHERE `id` = ?;";

    private static final String SQL__UPDATE_SUBJECT_LANG_SET =
                    "UPDATE subjects_languages SET name = ?\n" +
                    "WHERE subjects_id = ? AND languages_id = ?;";

    private static final String SQL__DELETE_SUBJECT =
            "DELETE FROM subjects WHERE id = ?;";

    /**
     * Returns subject by id
     * @param id of subject
     * @param params includes locales data
     * @return subject
     */
    @Override
    public Subject getById(int id, String[] params) {
        Subject subject = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            SubjectMapper mapper = new SubjectMapper();
            pstmt = con.prepareStatement(SQL__SELECT_SUBJECTS_BY_ID);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()){
                subject = mapper.mapRow(rs);
            }

            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return subject;
    }

    /**
     * Returns all subjects depending on locale language.
     * @return List of subjects entities.
     */
    @Override
    public List<Subject> getAll(String[] params) {
        log.debug("getAll --> started");
        List<Subject> subjectsList = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        if (params == null || params.length == 0)
            return subjectsList;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__SELECT_ALL_SUBJECTS);
            pstmt.setString(1, params[0]);
            rs = pstmt.executeQuery();
            SubjectMapper mapper = new SubjectMapper();
            while (rs.next())
                subjectsList.add(mapper.mapRow(rs));
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("getAll size() --> " + subjectsList.size());
        return subjectsList;
    }

    /**
     * Returns all subjects by faculty id.
     * @param params
     * @returnList of subjects entities.
     */
    public List<Subject> getAllByFacultyId(String[] params) {
        log.debug("getAllByFacultyId --> started");
        List<Subject> subjectsList = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        if (params == null || params.length == 0)
            return subjectsList;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__SELECT_SUBJECTS_BY_FACULTY_ID);
            pstmt.setString(1, params[0]);
            pstmt.setInt(2, Integer.parseInt(params[1]));
            rs = pstmt.executeQuery();
            SubjectMapper mapper = new SubjectMapper();
            log.debug("getAllByFacultyId --> before mapping");
            while (rs.next())
                subjectsList.add(mapper.mapRow(rs));
        } catch (SQLException ex) {
            log.debug("getAllByFacultyId exception --> " + ex.getMessage());
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("getAllByFacultyId size() --> " + subjectsList.size());
        return subjectsList;
    }

    /**
     * Insert subject into db
     * @param subject instance
     * @param params additional data
     * @return inserted row id
     */
    @Override
    public int create(Subject subject, String[] params) {
        PreparedStatement pstmt;
        Connection con = null;
        int createdId = 0;
        log.debug("create before connection subj -->" + subject.getNameList().get(0));
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__INSERT_SUBJECT, Statement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, subject.getCourseDuration());

            if (pstmt.executeUpdate() == 0) {
                throw new SQLException("Creating subject failed, no rows affected.");
            }
            try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    createdId = generatedKeys.getInt(1);
                } else {
                    throw new SQLException("Creating subject failed, no ID obtained.");
                }
            }

            pstmt = con.prepareStatement(SQL__INSERT_SUBJECT_LANG);
            pstmt.setString(1, subject.getNameList().get(0));
            pstmt.setString(2, subject.getNameList().get(1));
            pstmt.setString(3, subject.getNameList().get(2));
            if (pstmt.executeUpdate() == 0) {
                throw new SQLException("Creating subject_language failed, no rows affected.");
            }

        } catch (SQLException throwables) {
            log.debug("Difficulties to get id --> " + throwables);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("create new subject name (en) -->" + subject.getNameList().get(0));
        return createdId;
    }

    /**
     * Update subject.
     * @param subject subject to update.
     */
    @Override
    public boolean update(Subject subject, String[] params) {

        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            updateSubject(con, subject, params);
            updateSubjectLangSet(con, subject, params);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            return false;
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return true;
    }

    /**
     * Update subject.
     * @param con connection data
     * @param subject to update
     * @param params other data params
     * @throws SQLException while update
     */
    public void updateSubject(Connection con, Subject subject, String[] params) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL__UPDATE_SUBJECT);
        pstmt.setInt(1, subject.getCourseDuration());
        pstmt.setInt(2, subject.getId());
        pstmt.executeUpdate();
        pstmt.close();
    }

    public void updateSubjectLangSet(Connection con, Subject subject, String[] params) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL__UPDATE_SUBJECT_LANG_SET);
        for(int i = 0; i < subject.getNameList().size(); i++){
            if(i == 0){
                pstmt.setString(1, subject.getNameList().get(i));
                pstmt.setInt(2, subject.getId());
                pstmt.setInt(3, 1);
            }else if(i == 1){
                pstmt.setString(1, subject.getNameList().get(i));
                pstmt.setInt(2, subject.getId());
                pstmt.setInt(3, 2);
            }else if(i == 2){
                pstmt.setString(1, subject.getNameList().get(i));
                pstmt.setInt(2, subject.getId());
                pstmt.setInt(3, 3);
            }
            pstmt.executeUpdate();
        }
        pstmt.close();
    }

    /**
     * Delete subject by id
     * @param id of db row
     * @return true if deleted
     */
    @Override
    public boolean delete(int id) {
        log.debug("delete started for id --> " + id);
        PreparedStatement pstmt;
        Connection con = null;
        if (id == 0 || id < 0)
            return false;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__DELETE_SUBJECT);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (Exception e) {
            log.trace("Subject is not deleted", e);
            return false;
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("delete result --> true");
        return true;
    }

    /**
     * Extracts an subject from the result set row.
     */
    private static class SubjectMapper implements EntityMapper<Subject> {
        @Override
        public Subject mapRow(ResultSet rs) {
            try {
                Subject subject = new Subject();
                subject.setId(rs.getInt(Fields.SUBJECT_ID));
                subject.setCourseDuration(rs.getInt(Fields.SUBJECT_DURATION));
                subject.getNameList().addAll(Arrays.asList(rs.getString(Fields.SUBJECT_NAME).split(Pattern.quote(";"))));
                return subject;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}