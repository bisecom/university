package dao;

import db.DBManager;
import db.Fields;
import model.Application;
import model.Enrollee;
import model.Faculty;
import org.apache.log4j.Logger;
import utils.ApplicationSearchChoice;
import utils.ApplicationStatus;
import utils.EntityMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Fulfill crud operations for Application entity with the db
 */
public class ApplicationDao implements Dao<Application> {
    private static final Logger log = Logger.getLogger(ApplicationDao.class);

    private static final String SQL__INSERT_APPL =
            "INSERT INTO applications(user_logins_id, faculties_id, priority, is_approved)VALUES(?, ?, ?, ?);";
    private static final String SQL__INSERT_SET_APPL_GRADE =
            "INSERT INTO applications_grades(applications_id, grades_id)VALUES(?,?);";
    private static final String SQL__GET_APPL_BY_ID =
            "SELECT ap.id, ap.user_logins_id, ap.faculties_id, ap.priority, ap.is_approved\n" +
                    "FROM applications ap\n" +
                    "WHERE ap.id = ?;";
    private static final String SQL__GET_ALL_APPL_BY_USER_ID =
            "SELECT ap.id, ap.faculties_id, fl.name, ap.priority, ap.is_approved\n" +
                    "FROM applications ap\n" +
                    "INNER JOIN faculties fa ON ap.faculties_id = fa.id\n" +
                    "INNER JOIN faculties_languages fl ON fl.faculties_id = fa.id\n" +
                    "INNER JOIN languages la ON la.id = fl.languages_id\n" +
                    "WHERE ap.user_logins_id = ? AND la.lang_code = ?;";

    private static final String SQL__GET_ALL_APPL_BY_FACULTY_ID =
    "SELECT ap.id, ap.user_logins_id, en.second_name, en.first_name, ap.priority, ap.is_approved\n" +
            "FROM applications ap\n" +
            "INNER JOIN user_logins ul ON ul.id = ap.user_logins_id\n" +
            "INNER JOIN enrollees en ON en.user_logins_id = ul.id\n" +
            "WHERE ap.faculties_id = ?;";

    private static final String SQL__UPDATE_APPL_BY_ID =
            "UPDATE applications SET priority = ?, is_approved = ? WHERE id = ?;";
    private static final String SQL__DELETE_APPL_BY_ID =
            "DELETE FROM applications WHERE id = ?;";

    /**
     * GEt application entity from db
     * @param id of entity
     * @param params locales data
     * @return entity by id
     */
    @Override
    public Application getById(int id, String[] params) {
        Application application = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            ApplicationMapper mapper = new ApplicationMapper();
            pstmt = con.prepareStatement(SQL__GET_APPL_BY_ID);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()){
                application = mapper.mapRow(rs);
            }
            //upload faculty data
            Faculty faculty = new FacultyDao().getById(application.getFaculty().getId(), new String[]{});
            application.setFaculty(faculty);

            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return application;
    }

    /**
     * Get list of applications placed by enrollee OR faculty
     * @param params provide enrollee id and locale data
     * @return list of applications entities
     */
    @Override
    public List<Application> getAll(String[] params) {
        log.debug("getAllByEnrolleeId --> started");
        List<Application> applicationsList = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        if (params == null || params.length == 0)
            return applicationsList;

        try {
            con = DBManager.getInstance().getConnection();
            if (params[2].equals(ApplicationSearchChoice.ENROLLEE.toString())) {
                pstmt = con.prepareStatement(SQL__GET_ALL_APPL_BY_USER_ID);
                pstmt.setInt(1, Integer.parseInt(params[1]));
                pstmt.setString(2, params[0]);

                rs = pstmt.executeQuery();
                ApplicationMapper mapper = new ApplicationMapper();
                log.debug("getAllByEnrolleeId user --> before mapping");
                Application appl = null;
                while (rs.next()) {
                    appl = mapper.mapRow(rs);
                    log.debug("appl = mapper.mapRow(rs)--> " + appl);
                    if (appl != null) {
                        applicationsList.add(appl);
                    }
                }
            } else {
                pstmt = con.prepareStatement(SQL__GET_ALL_APPL_BY_FACULTY_ID);
                pstmt.setInt(1, Integer.parseInt(params[1]));

                rs = pstmt.executeQuery();
                ApplicationMapper mapper = new ApplicationMapper();
                log.debug("getAllByFacultyId faculty --> before mapping");
                while (rs.next())
                    applicationsList.add(mapper.mapRowStmt(rs));
            }
            if (applicationsList.size() > 0) {
                for (Application a : applicationsList) {
                    a.getGradesList().addAll(new GradeDao().getAll(new String[]{params[0], String.valueOf(a.getId())}));
                }
                log.debug("appl.getGradesList() --> " + applicationsList.get(0).getGradesList().size());
            }
        } catch (SQLException ex) {
            log.debug("getAllByEnrolleeId exception --> " + ex.getMessage());
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("getAllApplications size() --> " + applicationsList.size());
        return applicationsList;
    }

    /**
     * Insert application entity into db
     * @param application entity to save
     * @param params locale data
     * @return id from the table
     */
    @Override
    public int create(Application application, String[] params) {
        PreparedStatement pstmt;
        Connection con = null;
        int createdId = 0;
        log.debug("create before connection application for enrollee -->" + application.getEnrollee().getId());
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__INSERT_APPL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, application.getEnrollee().getId());
            pstmt.setInt(2, application.getFaculty().getId());
            pstmt.setInt(3, application.getPriority());
            pstmt.setInt(4, application.getApplicationStatus().ordinal());

            if (pstmt.executeUpdate() == 0) {
                throw new SQLException("Creating application failed, no rows affected.");
            }
            try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    createdId = generatedKeys.getInt(1);
                } else {
                    throw new SQLException("Creating application failed, no ID obtained.");
                }
            }

        } catch (SQLException throwables) {
            log.debug("Difficulties to get id --> " + throwables);
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("create new application for enrollee -->" + application.getEnrollee().getId());
        return createdId;
    }

    /**
     * Update entity in the db
     * @param application entity
     * @param params locale details
     * @return true if succeed
     */
    @Override
    public boolean update(Application application, String[] params) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            updateApplication(con, application, params);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            return false;
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return true;
    }

    /**
     * Update application in the db.
     * @param con entity to use
     * @param application entity to update
     * @param params locale data
     * @throws SQLException while update
     */
    public void updateApplication(Connection con, Application application, String[] params) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL__UPDATE_APPL_BY_ID);
        int k = 1;
        pstmt.setInt(k++, application.getPriority());
        pstmt.setInt(k++, application.getApplicationStatus().ordinal());
        pstmt.setInt(k, application.getId());
        pstmt.executeUpdate();
        pstmt.close();
    }

    /**
     * Delete application entity from the db
     * @param id of application entity
     * @return true if succeed
     */
    @Override
    public boolean delete(int id) {
        log.debug("delete appl started for id --> " + id);
        PreparedStatement pstmt;
        Connection con = null;
        if (id == 0 || id < 0)
            return false;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__DELETE_APPL_BY_ID);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (Exception e) {
            log.trace("Application is not deleted", e);
            return false;
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        log.debug("delete appl result --> true");
        return true;
    }

    /**
     * Extracts an application entity from the result set row.
     */
    private static class ApplicationMapper implements EntityMapper<Application> {
        @Override
        public Application mapRow(ResultSet rs) {
            try {
                Application application = new Application();
                //Enrollee enrollee = new Enrollee();
                Faculty faculty = new Faculty();
                application.setFaculty(faculty);
                //application.setEnrollee(enrollee);

                application.setId(rs.getInt(Fields.APPL_ID));
                application.getFaculty().setId(rs.getInt("faculties_id"));
                application.getFaculty().getNamesList().add(rs.getString(Fields.FACULTY_NAME));
                application.setPriority(rs.getInt(Fields.APPL_PRIORITY));
                application.setApplicationStatus(ApplicationStatus.values()[rs.getInt(Fields.APPL_IS_APPROVED)]);
                return application;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
        public Application mapRowStmt(ResultSet rs) {
            try {
                Application application = new Application();
                Enrollee enrollee = new Enrollee();
                Faculty faculty = new Faculty();
                application.setFaculty(faculty);
                application.setEnrollee(enrollee);

                application.setId(rs.getInt(Fields.APPL_ID));
                application.getEnrollee().setId(rs.getInt(Fields.USER_ID));
                application.getEnrollee().setSecondName(rs.getString(Fields.USER_SECOND_NAME));
                application.getEnrollee().setFirstName(rs.getString(Fields.USER_FIRST_NAME));
                application.setPriority(rs.getInt(Fields.APPL_PRIORITY));
                application.setApplicationStatus(ApplicationStatus.values()[rs.getInt(Fields.APPL_IS_APPROVED)]);
                return application;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
