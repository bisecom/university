package command;

import dao.SubjectDao;
import model.Subject;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Get updated paraps of subject from request and update the subject in the db
 */
public class UpdateSubjectCommand extends Command {
    private static final long serialVersionUID = 6508060067451474356L;
    private static final Logger log = Logger.getLogger(UpdateSubjectCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        log.debug("UpdateSubjectCommand starts");
        HttpSession session = request.getSession();
        Subject subjectToUpdate = new Subject();
        String subjIdToUpdate = request.getParameter("updateSubjectValue");

        String subjId = request.getParameter("subjId");
        String courseDuration = request.getParameter("courseDuration");

        String subjEngName = request.getParameter("subjEngName");
        String subjUkrName = request.getParameter("subjUkrName");
        String subjRusName = request.getParameter("subjRusName");

        // error handler
        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;
        log.debug("UpdateSubjectCommand subjIdToUpdate value --> " + subjIdToUpdate);
        if (subjId == null && courseDuration == null && subjEngName == null && subjUkrName == null && subjRusName == null &&
                subjIdToUpdate == null) {
            errorMessage = "Required fields cannot be empty";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        if(subjIdToUpdate != null && subjId == null)
        {
            Subject subToDisplay = new SubjectDao().getById(Integer.parseInt(subjIdToUpdate), new String[]{});
            session.setAttribute("subToDisplay", subToDisplay);
            forward = Path.PAGE__ADMIN_SUBJECTS_UPDATE;
            return forward;
        }

        subjectToUpdate.setId(Integer.parseInt(subjId));
        subjectToUpdate.setCourseDuration(Integer.parseInt(courseDuration));
        subjectToUpdate.getNameList().add(subjEngName);
        subjectToUpdate.getNameList().add(subjUkrName);
        subjectToUpdate.getNameList().add(subjRusName);

        log.debug("subjUkrName to write to db --> " + subjUkrName);
        log.debug("subjRusName to write to db --> " + subjRusName);
        boolean isUpdated = new SubjectDao().update(subjectToUpdate, new String[]{});
        log.debug("Found in DB: subject --> " + isUpdated);
        if (!isUpdated) {
            errorMessage = "Cannot update subject!";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return Path.PAGE__ERROR_PAGE;
        }

        forward = Path.COMMAND__LIST_SUBJECTS_BOARD;
        log.debug("UpdateSubjectCommand finished with --> " + forward);
        return forward;
    }
}