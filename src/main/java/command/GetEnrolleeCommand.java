package command;

import dao.ApplicationDao;
import dao.EnrolleeDao;
import model.Application;
import model.Enrollee;
import org.apache.log4j.Logger;
import utils.ApplicationSearchChoice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Base64;
import java.util.List;

/**
 * Get enrollee details from db and forward to jsp
 */
public class GetEnrolleeCommand extends Command {
    private static final long serialVersionUID = 1784706413137856151L;
    private static final Logger log = Logger.getLogger(GetEnrolleeCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("GetEnrolleeCommand starts");
        Enrollee enrollee;
        HttpSession session = request.getSession();
        String language = (String) session.getAttribute("elanguage");
        // error handler
        String errorMessage;
        String forward = Path.PAGE__ERROR_PAGE;

        String enrolleeId = request.getParameter("selectedEnrolleeId");
        log.debug("EnrolleeId value --> " + enrolleeId);

        if (enrolleeId == null || enrolleeId.isEmpty()) {
            errorMessage = "Could not get enrollee";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        } else {
            enrollee = new EnrolleeDao().getById(Integer.parseInt(enrolleeId), new String[]{language});
            List<Application> applicationsList = new ApplicationDao().getAll(new String[]{language, enrolleeId, ApplicationSearchChoice.ENROLLEE.toString()});

            session.setAttribute("enrollee", enrollee);
            session.setAttribute("applicationsList", applicationsList);

            String certImage = Base64.getEncoder().encodeToString(enrollee.getSchoolCertificate());
            session.setAttribute("certImage", certImage);

            forward = Path.PAGE__ENROLLEE_DETAILS;
            log.debug("GetEnrolleeCommand finished with --> " + forward);
            return forward;
        }
    }
}
