package command;

import dao.ApplicationDao;
import dao.EnrolleeDao;
import dao.FacultyDao;
import dao.GradeDao;
import model.*;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import utils.ApplicationStatus;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Receive data from a form of application and insert it into the db
 */
@MultipartConfig(maxFileSize = 1024 * 1024 * 3)
public class InsertApplicationCommand extends Command {

    private static final long serialVersionUID = 3727177042074666240L;
    private static final Logger log = Logger.getLogger(InsertApplicationCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("InsertApplicationCommand started --> ");
        HttpSession session = request.getSession();
        Enrollee enrollee = (Enrollee) session.getAttribute("user");
        String language = (String)session.getAttribute("elanguage");
        log.debug("InsertApplicationCommand session language --> " + language);
        // Check that we have a file upload request
        // error handler
        String errorMessage;
        String forward = Path.PAGE__ERROR_PAGE;

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);

        List<Faculty> facultiesList = new FacultyDao().getAll(new String[]{language});
        log.debug("InsertApplicationCommand to board facultiesList size --> " + facultiesList.size());
        log.debug("InsertApplicationCommand getSubjectList().get(0) --> " + facultiesList.get(0).getSubjectList().get(0));
        session.setAttribute("facultiesList", facultiesList);

        String selectFacultyPriority1 = request.getParameter("selectedFaculty1");
        String selectFacultyPriority2 = request.getParameter("selectedFaculty2");
        String selectFacultyPriority3 = request.getParameter("selectedFaculty3");

        log.debug("selectFacultyPriority1 --> " + selectFacultyPriority1);
        log.debug("selectFacultyPriority2 --> " + selectFacultyPriority2);
        log.debug("selectFacultyPriority3 --> " + selectFacultyPriority3);

        if (selectFacultyPriority1 == null && selectFacultyPriority2 == null &&
                selectFacultyPriority3 == null && !isMultipart) {
            session.setAttribute("priority1Subjects", 1);
            session.setAttribute("priority2Subjects", 2);
            session.setAttribute("priority3Subjects", 3);
            forward = Path.PAGE__ENROLLEE_INSERT_APPL;
            return forward;
        }
        if (selectFacultyPriority1 != null && selectFacultyPriority2 != null &&
                selectFacultyPriority3 != null && !isMultipart) {
            session.setAttribute("priority1Subjects", selectFacultyPriority1);
            session.setAttribute("priority2Subjects", selectFacultyPriority2);
            session.setAttribute("priority3Subjects", selectFacultyPriority3);
            forward = Path.PAGE__ENROLLEE_INSERT_APPL;
            return forward;
        }
        if (isMultipart) {
            // Create a factory for disk-based file items
            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(1024 * 1024 * 4);
            factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
            ServletFileUpload upload = new ServletFileUpload(factory);

            try {
                Application application1 = new Application();
                Application application2 = new Application();
                Application application3 = new Application();
                List<Grade> gradesList1 = new ArrayList<>();
                List<Grade> gradesList2 = new ArrayList<>();
                List<Grade> gradesList3 = new ArrayList<>();
                byte[] uploadedCertificate = null;
                // Parse the request
                List items = upload.parseRequest(request);
                Iterator iter = items.iterator();
                while (iter.hasNext()) {
                    FileItem item = (FileItem) iter.next();

                    if (!item.isFormField()) {
                        uploadedCertificate = IOUtils.toByteArray(item.getInputStream());
                        log.debug("fileContent size --> " + uploadedCertificate.length);
                    } else {
                        String fieldvalue = item.getString();
                        log.debug("fieldtext + fieldvalue  --> " + item.getFieldName() + " " + fieldvalue);
                        if (item.getFieldName().contains("priority1")) {
                            gradesList1.add(new Grade(new Subject(Integer.parseInt(item.getFieldName().replaceAll("priority[0-9]subj", ""))) {
                            }, Integer.parseInt(item.getString())) {
                            });
                        }
                        if (item.getFieldName().contains("priority2")) {
                            gradesList2.add(new Grade(new Subject(Integer.parseInt(item.getFieldName().replaceAll("priority[0-9]subj", ""))) {
                            }, Integer.parseInt(item.getString())) {
                            });
                        }
                        if (item.getFieldName().contains("priority3")) {
                            gradesList3.add(new Grade(new Subject(Integer.parseInt(item.getFieldName().replaceAll("priority[0-9]subj", ""))) {
                            }, Integer.parseInt(item.getString())) {
                            });
                        }

                        if ("Priority1Faculty".equals(item.getFieldName())) {
                            int facultyId = Integer.parseInt(item.getString());
                            application1.setFaculty(new Faculty(facultyId) {
                            });
                            application1.setEnrollee(new Enrollee(enrollee.getId()));
                            application1.setPriority(1);
                            application1.setApplicationStatus(ApplicationStatus.NOT_PROCEED);
                        }
                        if ("Priority2Faculty".equals(item.getFieldName())) {
                            int facultyId = Integer.parseInt(item.getString());
                            application2.setFaculty(new Faculty(facultyId) {
                            });
                            application2.setEnrollee(new Enrollee(enrollee.getId()));
                            application2.setPriority(2);
                            application2.setApplicationStatus(ApplicationStatus.NOT_PROCEED);
                        }
                        if ("Priority3Faculty".equals(item.getFieldName())) {
                            int facultyId = Integer.parseInt(item.getString());
                            application3.setFaculty(new Faculty(facultyId) {
                            });
                            application3.setEnrollee(new Enrollee(enrollee.getId()));
                            application3.setPriority(3);
                            application3.setApplicationStatus(ApplicationStatus.NOT_PROCEED);
                        }
                    }
                }
                application1.getGradesList().addAll(gradesList1);
                application2.getGradesList().addAll(gradesList2);
                application3.getGradesList().addAll(gradesList3);

                int insertedAppl1 = new ApplicationDao().create(application1, new String[]{});
                new GradeDao().createGradesSet(application1, new String[]{language, String.valueOf(insertedAppl1)});

                int insertedAppl2 = new ApplicationDao().create(application2, new String[]{});
                new GradeDao().createGradesSet(application2, new String[]{language, String.valueOf(insertedAppl2)});

                int insertedAppl3 = new ApplicationDao().create(application3, new String[]{});
                new GradeDao().createGradesSet(application3, new String[]{language, String.valueOf(insertedAppl3)});

                boolean isCertificateInserted = new EnrolleeDao().updateCertificate(enrollee.getId(), uploadedCertificate, new String[]{});
                if (isCertificateInserted) {
                    String certImage = Base64.getEncoder().encodeToString(uploadedCertificate);
                    session.setAttribute("certImage", certImage);
                }
                log.debug("isCertificateInserted --> " + isCertificateInserted);
                log.debug("application1.getApplicationStatus() --> " + application1.getApplicationStatus());
            } catch (Exception ex) {
                throw new ServletException(ex);
            }
            forward = Path.COMMAND__ENROLLEE_PANEL;
        }
        log.debug("InsertApplicationCommand finished with --> " + forward);
        return forward;
    }
}