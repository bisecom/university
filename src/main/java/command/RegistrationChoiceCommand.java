package command;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Redirect request to registration jsp
 */
public class RegistrationChoiceCommand extends Command{
    private static final long serialVersionUID = 1666483864804192503L;
    private static final Logger log = Logger.getLogger(LoginCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("RegistrationChoiceCommand finished with --> " + Path.PAGE__REGISTRATION);
        return Path.PAGE__REGISTRATION;
    }
}