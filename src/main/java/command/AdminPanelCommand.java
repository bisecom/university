package command;

import dao.EnrolleeDao;
import model.Enrollee;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;


public class AdminPanelCommand extends Command{
    private static final long serialVersionUID = 688942586252025570L;
    private static final Logger log = Logger.getLogger(AdminPanelCommand.class);

    /**
     * Serializable comparator used with TreeMap container. When the servlet
     * container tries to serialize the session it may fail because the session
     * can contain TreeMap object with not serializable comparator.
     *
     * @author Sergiy B.
     *
     */
    private static class CompareById implements Comparator<Enrollee>, Serializable {
        private static final long serialVersionUID = 3913766954021820813L;

        public int compare(Enrollee user1, Enrollee user2) {
            if (user1.getId() > user2.getId())
                return 1;
            else return -1;
        }
    }

    private static Comparator<Enrollee> compareById = new CompareById();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("AdminPanelCommand starts");
        HttpSession session = request.getSession();
        String language = (String) session.getAttribute("elanguage");
        List<Enrollee> userOrderBeanList = new EnrolleeDao().getAll(new String[]{language});
        log.trace("Found in DB: userOrderBeanList --> " + userOrderBeanList);

        userOrderBeanList.sort(compareById);

        // put user order beans list to request
        request.setAttribute("userOrderBeanList", userOrderBeanList);
        log.trace("Set the request attribute: userOrderBeanList --> " + userOrderBeanList);

        log.debug("AdminPanelCommand finished");
        return Path.PAGE__ADMIN_PANEL;
    }

}