package command;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

/**
 * Filfill logout command and invalidate session.
 * @author Sergiy B.
 */
public class LogoutCommand extends Command {
    private static final long serialVersionUID = 7072355557139756496L;
    private static final Logger log = Logger.getLogger(LogoutCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("LogoutCommand starts");

        HttpSession session = request.getSession(false);
        if (session != null)
            session.invalidate();

        log.debug("LogoutCommand finished");
        return Path.PAGE__INDEX;
    }

}