package command;

public final class Path {
    // pages
    public static final String PAGE__INDEX = "/view/index.jsp";
    public static final String PAGE__LOGIN = "/view/login/login.jsp";
    public static final String PAGE__REGISTRATION = "/view/login/registration.jsp";
    public static final String PAGE__FINAL_REGISTRATION = "/view/login/registrationFinal.jsp";
    public static final String PAGE__ERROR_PAGE = "/jsp/error_page.jsp";
    public static final String PAGE__ADMIN_PANEL = "/view/admin/adminDashBoard.jsp";
    public static final String PAGE__ADMIN_FACULTIES = "/view/admin/faculties/facultiesDashBoard.jsp";
    public static final String PAGE__ADMIN_SUBJECTS = "/view/admin/subjects/subjectDashBoard.jsp";
    public static final String PAGE__ADMIN_SUBJECTS_UPDATE = "/view/admin/subjects/updateSubject.jsp";
    public static final String PAGE__ENROLLEE_INSERT_APPL = "/view/user/enrolleeInsertAppl.jsp";
    public static final String PAGE__ENROLLEE_DISPLAY_PROFILE = "/view/user/enrolleeDisplayProfile.jsp";
    public static final String PAGE__CREATE_FACULTY = "/view/admin/faculties/createFaculty.jsp";
    public static final String PAGE__ADMIN_FACULTIES_UPDATE = "/view/admin/faculties/updateFaculty.jsp";
    public static final String PAGE__ENROLLEE_CONTROL_DASH = "/view/admin/enrollees/enrolleesControlDashBoard.jsp";
    public static final String PAGE__ENROLLEE_DETAILS = "/view/admin/enrollees/getEnrollee.jsp";
    public static final String PAGE__STATEMENT_CONTROL_DASH = "/view/admin/statements/statementsDashBoard.jsp";

    // commands
    public static final String COMMAND__ADMIN_PANEL = "/controller?command=adminPanel";
    public static final String COMMAND__ENROLLEE_PANEL = "/controller?command=enrolleePanel";
    public static final String COMMAND__INSERT_ENROLLEE_APPLICATION = "/controller?command=insertEnrolleApplication";
    public static final String COMMAND__LIST_SUBJECTS_BOARD = "/controller?command=subjectDashBoard";
    public static final String COMMAND__FACULTIES_BOARD = "/controller?command=facultyDashBoard";
    public static final String COMMAND__ENROLLEES_CONTROL_DASH = "/controller?command=getEnrolleesControlDash";

}