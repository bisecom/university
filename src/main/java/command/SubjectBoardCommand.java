package command;

import dao.SubjectDao;
import model.Subject;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Retrieve list of subjects from db and forward it to jsp
 */
public class SubjectBoardCommand extends Command {
    private static final long serialVersionUID = 1467309152058201532L;
    private static final Logger log = Logger.getLogger(SubjectBoardCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("SubjectBoardCommand starts -->" + request);
        String forward = Path.PAGE__ADMIN_SUBJECTS;
        HttpSession session = request.getSession();
        String language = (String) session.getAttribute("language");
        List<Subject> subjectsList = new SubjectDao().getAll(new String[]{language});
        log.debug("SubjectsList to admin dash size --> " +  subjectsList.size());
        session.setAttribute("subjectsList", subjectsList);

        log.debug("SubjectBoardCommand finished with --> " + forward);
        return forward;
    }
}