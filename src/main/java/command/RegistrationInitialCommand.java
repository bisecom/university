package command;

import dao.EnrolleeDao;
import model.Enrollee;
import org.apache.log4j.Logger;
import utils.Helper;
import utils.Role;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Reception login and pass from user for initial registration.
 * Proposed 2 steps registration. After the first(fast) step user
 * is informed if such login exists.
 * Login is hashed via SHA-256. The login data inserted to the db
 */
public class RegistrationInitialCommand extends Command {
    private static final long serialVersionUID = -171209361031636786L;
    private static final Logger log = Logger.getLogger(RegistrationInitialCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        log.debug("RegistrationInitialCommand starts");
        HttpSession session = request.getSession();

        // obtain login and password from the request
        String email = request.getParameter("email");
        log.trace("Request parameter: email --> " + email);

        String password = request.getParameter("password");

        // error handler
        String errorMessage;
        String forward = Path.PAGE__ERROR_PAGE;

        if (email == null || password == null || email.isEmpty() || password.isEmpty()) {
            errorMessage = "Login/password cannot be empty";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        //getting password Hesh
        password = Helper.getPasswordHash(password);

        Enrollee user = new EnrolleeDao().getByLogin(email);
        log.trace("Found in DB: user --> " + user.getEmail());
        if (user.getEmail() != null && user.getId() != 0) {
            errorMessage = "Such user already exists!!";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return Path.PAGE__ERROR_PAGE;
        } else {
            user = new EnrolleeDao().createEnrollee(email, password);
            if(user == null){
                errorMessage = "Could not insert the User to the system!";
                log.error("errorMessage --> " + errorMessage);
                return Path.PAGE__ERROR_PAGE;
            }
            session.setAttribute("user", user);
            log.trace("Set the session attribute: user --> " + user);

            Role userRole = user.getRole();
            session.setAttribute("userRole", userRole);
            log.trace("Set the session attribute: userRole --> " + userRole);
            log.info("User " + user + " logged as " + user.getEmail());

            forward = Path.PAGE__FINAL_REGISTRATION;
        }
        log.debug("RegistrationInitialCommand finished with --> " + forward);
        return forward;
    }
}