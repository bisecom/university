package command;

import dao.SubjectDao;
import model.Subject;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Retrieve list of faculty entities from the db and forward to
 * faculty dashboard page or create new faculty page
 */
public class ShowFacultyFormCommand extends Command{
    private static final long serialVersionUID = 6434454833823103455L;
    private static final Logger log = Logger.getLogger(ShowFacultyFormCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("ShowFacultyFormCommand starts");

        HttpSession session = request.getSession();
        String language = (String) session.getAttribute("elanguage");
        // error handler
        String errorMessage;
        String forward;

        List<Subject> subjectsList = new SubjectDao().getAll(new String[]{language});
        log.debug("Found subjects in DB: size --> " + subjectsList.size());
        if (subjectsList.size() == 0) {
            errorMessage = "Cannot find subjects";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return Path.PAGE__ADMIN_FACULTIES;
        } else {
            session.setAttribute("subjectsList", subjectsList);
            log.trace("Set the session attribute: subjectsList --> " + subjectsList);
            forward = Path.PAGE__CREATE_FACULTY;
        }
        log.debug("ShowFacultyFormCommand finished with --> " + forward);
        return forward;
    }
}
