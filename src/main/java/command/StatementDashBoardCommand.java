package command;

import dao.ApplicationDao;
import dao.FacultyDao;
import model.Application;
import model.Faculty;
import org.apache.log4j.Logger;
import utils.ApplicationSearchChoice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Retrieve list of application depending on requested faculty
 */
public class StatementDashBoardCommand extends Command {
    private static final long serialVersionUID = 7737751233574483424L;
    private static final Logger log = Logger.getLogger(StatementDashBoardCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("StatementDashBoardCommand starts");
        List<Application> applicationsList;
        HttpSession session = request.getSession();
        String language = (String) session.getAttribute("elanguage");
        // error handler

        String sFaculty = request.getParameter("selectedFacultyStm");

        if(sFaculty == null || sFaculty.isEmpty()){
            applicationsList = new ApplicationDao().getAll(new String[]{language, "1", ApplicationSearchChoice.FACULTY.toString()});
        }else {
            applicationsList = new ApplicationDao().getAll(new String[]{language, sFaculty, ApplicationSearchChoice.FACULTY.toString()});
            session.setAttribute("selectValue", sFaculty);
        }
        List<Faculty> facultiesList = new FacultyDao().getAll(new String[]{language});

        session.setAttribute("applicationsList", applicationsList);
        log.trace("Set the session attribute: applicationsList --> " + applicationsList);

        session.setAttribute("facultiesList", facultiesList);
        log.trace("Set the session attribute: facultiesList --> " + facultiesList);

        String forward = Path.PAGE__STATEMENT_CONTROL_DASH;
        log.debug("StatementDashBoardCommand finished with --> " + forward);
        return forward;
    }
}
