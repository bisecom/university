package command;

import dao.ApplicationDao;
import dao.FacultyDao;
import model.Application;
import model.Faculty;
import model.Grade;
import org.apache.log4j.Logger;
import utils.ApplicationSearchChoice;
import utils.ApplicationStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Fulfill applications from db
 * and proceed them according to available
 * budget and total places for every faculty.
 * Change status for every application if it is
 * approved to budget, contract or not approved.
 */
public class CreateStatementCommand extends Command {
    private static final long serialVersionUID = 5334708660357072520L;
    private static final Logger log = Logger.getLogger(CreateStatementCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("CreateStatementCommand starts -->" + request);
        String forward = Path.PAGE__STATEMENT_CONTROL_DASH;

        HttpSession session = request.getSession();
        String language = (String) session.getAttribute("elanguage");

        List<Faculty> facultiesList = new FacultyDao().getAll(new String[]{language});

        List<List<Application>> applicationsList = getApplsByFaculies(facultiesList, language);
        log.debug("List<List<Application>> size --> " + applicationsList.size());

        for (List<Application> aplls : applicationsList) {
            aplls.sort(Collections.reverseOrder(Application.COMPARE_BY_AVERAGE_GRADE));
        }

        Map<Integer, int[]> mapFacultyLimitGrades = getGradeLimits(applicationsList);

        for (List<Application> aplls : applicationsList) {
            for (Application a : aplls) {
                putApplsStatus(a, mapFacultyLimitGrades);
            }
        }

        boolean isStatementComplete = putApplsChangesToDb(applicationsList);
        if (!isStatementComplete) {
            return Path.PAGE__ERROR_PAGE;
        }

        log.debug("applicationsList to board size --> " +  applicationsList.size());
        session.setAttribute("applicationsList", applicationsList.get(0));

        log.debug("CreateStatementCommand finished with --> " + forward);
        return forward;
    }

    /**
     * The method calculate average grade for the application
     * and compare it to min grade level enough to be approved to
     * budget or contract.
     * @param a Application entity
     * @param mapFacultyLimitGrades Map has keys as faculties Ids and
     *       int array as budget and total places for every faculty
     */
    private void putApplsStatus(Application a, Map<Integer, int []> mapFacultyLimitGrades){
            int facultyId = a.getFaculty().getId();
            float averageGrade = getAveregeGrade(a.getGradesList());
        //higher then budget min level
            if (mapFacultyLimitGrades.containsKey(facultyId)
                    && averageGrade >= mapFacultyLimitGrades.get(facultyId)[0]) {
                a.setApplicationStatus(ApplicationStatus.BUDGET_APPROVED);
            }
            //higher then contract min level but less then budget level
            if (mapFacultyLimitGrades.containsKey(facultyId)
                    && averageGrade >= mapFacultyLimitGrades.get(facultyId)[1]
            && averageGrade < mapFacultyLimitGrades.get(facultyId)[0]) {
                a.setApplicationStatus(ApplicationStatus.CONTRACT_APPROVED);
            }
            //less then min contract level - rejects
            if (mapFacultyLimitGrades.containsKey(facultyId)
                    && averageGrade < mapFacultyLimitGrades.get(facultyId)[1]) {
                a.setApplicationStatus(ApplicationStatus.NOT_APPROVED);
        }
    }

    /**
     * The method calculates average grade for list of Grades
     * @param gradeList List of Grades at every Application
     * @return average grade
     */
    private float getAveregeGrade(List<Grade> gradeList) {
        if (gradeList == null && gradeList.isEmpty()) {
            return 0;
        }
        float aver = 0f;
        for (Grade g : gradeList) {
            aver += g.getGrade();
        }
        return aver / gradeList.size();
    }

    /**
     * Create Map with key as Faculty id and List first element min
     * grade to reach budget and second element - min grade to reach contract
     * @param applicationsList lists of applications by each faculty
     * @return map of limit grades by every faculty
     */
    private Map<Integer, int []> getGradeLimits(List<List<Application>> applicationsList){
        Map<Integer, int []> mapFacultyLimitGrades = new HashMap<>();

        for (List<Application> aplls : applicationsList) {
            //first case
            if (aplls.size() <= aplls.get(0).getFaculty().getBudgetPlacesQty()) {
                mapFacultyLimitGrades.put(aplls.get(0).getFaculty().getId(), new int[]{0,0});
            }
            //second case
            if (aplls.size() <= aplls.get(0).getFaculty().getTotalPlacesQty() &&
                    aplls.size() > aplls.get(0).getFaculty().getBudgetPlacesQty()) {

                float minGradeToBudget =  getAveregeGrade(aplls.get(aplls.get(0).getFaculty().getBudgetPlacesQty()).getGradesList());
                mapFacultyLimitGrades.put(aplls.get(0).getFaculty().getId(), new int[]{Math.round(minGradeToBudget),0});
            }
            //third case
            if(aplls.size() > aplls.get(0).getFaculty().getTotalPlacesQty()){
                float minGradeToBudget =  getAveregeGrade(aplls.get(aplls.get(0).getFaculty().getBudgetPlacesQty()).getGradesList());
                float minGradeToContract =  getAveregeGrade(aplls.get(aplls.get(0).getFaculty().getTotalPlacesQty()).getGradesList());
                mapFacultyLimitGrades.put(aplls.get(0).getFaculty().getId(), new int[]{Math.round(minGradeToBudget),Math.round(minGradeToContract)});
            }
        }
        return mapFacultyLimitGrades;
    }

    /**
     * The method get applications for every faculty
     * @param facultiesList list of Faculties
     * @param language to get faculties with requested language
     * @return list of applications
     */
    protected List<List<Application>> getApplsByFaculies(List<Faculty>facultiesList, String language){
        List<List<Application>> applicationsList = new ArrayList<>();
        log.debug("facultiesList size --> " + facultiesList.size());

        List<Thread> threads = new ArrayList<>();
        for (Faculty f : facultiesList) {
            Thread newThread = new Thread(() -> {
                List<Application> applicationList = new ApplicationDao().getAll(new String[]{language, String.valueOf(f.getId()), ApplicationSearchChoice.FACULTY.toString()});
                if(applicationList != null)
                    applicationsList.add(applicationList);
            });
            newThread.start();
            threads.add(newThread);
        }
        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return applicationsList;
    }

    /**
     * The method save to db complete list of applications with changed status
     * @param applicationsList list of applications
     * @return true if succeed
     */
    protected boolean putApplsChangesToDb(List<List<Application>> applicationsList){
        log.debug("List<List<Application>> size --> " + applicationsList.size());

        List<Thread> threads = new ArrayList<>();
        for (List<Application> aplls : applicationsList) {
            for (Application a : aplls) {
                Thread newThread = new Thread(() -> {
                    new ApplicationDao().update(a, new String[]{});
                });
                newThread.start();
                threads.add(newThread);
            }
        }

        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }
        }
        log.debug("putApplsChangesToDb finished with --> " + true);
        return true;
    }

}