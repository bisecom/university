package command;

import dao.FacultyDao;
import model.Faculty;
import model.Subject;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
 * Fulfill reception of data from a form
 * and insert into db
 */
public class CreateFacultyCommand extends Command {
    private static final long serialVersionUID = -7315485834974131481L;
    private static final Logger log = Logger.getLogger(CreateFacultyCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("CreateFacultyCommand starts -->" + request);

        Faculty faculty = new Faculty();
        // obtain data from the request
        String englishName = request.getParameter("englishName");
        log.debug("Request parameter: englishName --> " + englishName);
        String ukrainianName = request.getParameter("ukrainianName");
        log.trace("Request parameter: ukrainianName --> " + ukrainianName);
        String russianName = request.getParameter("russianName");
        log.trace("Request parameter: russianName --> " + russianName);
        String budgetQty = request.getParameter("budgetQty");
        log.trace("Request parameter: budgetQty --> " + budgetQty);
        String totalQty = request.getParameter("totalQty");
        log.trace("Request parameter: totalQty --> " + totalQty);
        String[] subjectsIds = request.getParameterValues("subject");
        log.debug("Request parameter: subject --> " + Arrays.toString(subjectsIds));

        // error handler
        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;

        if (englishName == null || ukrainianName == null || russianName == null || budgetQty == null ||
                totalQty == null || subjectsIds.length == 0 || englishName.isEmpty() || ukrainianName.isEmpty() ||
                russianName.isEmpty() || budgetQty.isEmpty() || totalQty.isEmpty()) {
            errorMessage = "Required fields cannot be empty";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        faculty.getNamesList().add(englishName);
        faculty.getNamesList().add(ukrainianName);
        faculty.getNamesList().add(russianName);
        faculty.setBudgetPlacesQty(Integer.parseInt(budgetQty));
        faculty.setTotalPlacesQty(Integer.parseInt(totalQty));
        for (String id : subjectsIds) {
            Subject s = new Subject();
            s.setId(Integer.parseInt(id));
            faculty.getSubjectList().add(s);
        }

        int facultyId = new FacultyDao().create(faculty, new String[]{});
        log.debug("CreateFacultyCommand create faculty with id --> " + facultyId);
        if (facultyId == 0) {
            errorMessage = "Required fields cannot be empty";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        forward = Path.COMMAND__FACULTIES_BOARD;
        log.debug("CreateFacultyCommand finished with --> " + forward);
        return forward;
    }
}
