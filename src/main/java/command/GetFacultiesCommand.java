package command;

import dao.FacultyDao;
import model.Faculty;
import org.apache.log4j.Logger;
import utils.Helper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.List;

/**
 * Retrieve list of faculties from db and sort it according to demand params
 */
public class GetFacultiesCommand extends Command {
    private static final long serialVersionUID = -7490635096350714850L;
    private static final Logger log = Logger.getLogger(GetFacultiesCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("GetFacultiesCommand starts -->" + request);
        String forward = Path.PAGE__INDEX;

        HttpSession session = request.getSession();
        String localeLang = request.getLocale().getLanguage();
        String language = (String) session.getAttribute("elanguage");
        //getting sorted filters
        String facultySort = request.getParameter("facultySort");
        log.debug("facultySort value -->" + facultySort);

        List<Faculty> facultiesList = new FacultyDao().getAll(new String[]{language == null ? localeLang : language});
        if(facultySort == null){
            Helper.getSortedList("nameAsc", facultiesList);
        }else {
            Helper.getSortedList(facultySort, facultiesList);
        }
        log.debug("FacultiesList to board size --> " +  facultiesList.size());
        session.setAttribute("facultiesList", facultiesList);

        log.debug("GetFacultiesCommand finished with --> " + forward);
        return forward;
    }
}