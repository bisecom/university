package command;

import dao.SubjectDao;
import model.Subject;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Fulfill reception of data from a form and insert it to db as Subject entity
 */
public class CreateSubjectCommand extends Command {
    private static final long serialVersionUID = 7731142443950954640L;
    private static final Logger log = Logger.getLogger(CreateSubjectCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("CreateSubjectCommand starts -->" + request);

        Subject subject = new Subject();
        // obtain data from the request
        String englishName = request.getParameter("englishName");
        log.debug("Request parameter: englishName --> " + englishName);
        String ukrainianName = request.getParameter("ukrainianName");
        log.trace("Request parameter: ukrainianName --> " + ukrainianName);
        String russianName = request.getParameter("russianName");
        log.trace("Request parameter: russianName --> " + russianName);

        String courseDuration = request.getParameter("courseDuration");
        log.trace("Request parameter: courseDuration --> " + courseDuration);

        // error handler
        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;

        if (englishName == null || ukrainianName == null || russianName == null ||
                courseDuration == null || englishName.isEmpty() || ukrainianName.isEmpty() ||
                russianName.isEmpty() || courseDuration.isEmpty()) {
            errorMessage = "Required fields cannot be empty";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        subject.setCourseDuration(Integer.parseInt(courseDuration));
        subject.getNameList().add(englishName);
        subject.getNameList().add(ukrainianName);
        subject.getNameList().add(russianName);

        int subjectId = new SubjectDao().create(subject, new String[]{});
        log.debug("CreateSubjectCommand create subject with id --> " + subjectId);
        if (subjectId == 0) {
            errorMessage = "Required fields cannot be empty";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        forward = Path.COMMAND__LIST_SUBJECTS_BOARD;
        log.debug("CreateSubjectCommand finished with --> " + forward);
        return forward;
    }
}
