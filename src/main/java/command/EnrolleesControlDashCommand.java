package command;

import dao.EnrolleeDao;
import dao.FacultyDao;
import model.Enrollee;
import model.Faculty;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Retrieve enrollee list from db according to provided parameters
 * (could be received for certain faculty and using pagination)
 */
public class EnrolleesControlDashCommand extends Command {
    private static final long serialVersionUID = 8269058788231232213L;
    private static final Logger log = Logger.getLogger(EnrolleesControlDashCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("EnrolleesControlDashCommand starts");
        List<Enrollee> enrolleesList = null;
        HttpSession session = request.getSession();
        String language = (String) session.getAttribute("elanguage");
        // error handler
        String errorMessage;
        String forward = Path.PAGE__ERROR_PAGE;

        Enrollee enrToUpdate = (Enrollee)session.getAttribute("enrollee");

        String isBlockedEnrollee = request.getParameter("isBlockedEnrollee");
        log.debug("isBlockedEnrollee value --> " + isBlockedEnrollee);

        if(isBlockedEnrollee != null && !isBlockedEnrollee.isEmpty() && enrToUpdate != null){
            enrToUpdate.setBlocked(Integer.parseInt(isBlockedEnrollee) == 1);
            new EnrolleeDao().update(enrToUpdate, new String[]{});
            log.debug("EnrolleesControlDashCommand within update --> " + enrToUpdate);
        }
        log.debug("EnrolleesControlDashCommand after update --> " + isBlockedEnrollee);

        String selectedFaculty = request.getParameter("selectedFaculty");
        String limitItemsQty = request.getParameter("limitItemsQty");
        String offSetValueEnrollees = request.getParameter("offSetValueEnrollees");

        log.debug("selectedFaculty --> " + selectedFaculty);
        log.debug("limitItemsQty --> " + limitItemsQty);
        log.debug("offSetValueEnrollees --> " + offSetValueEnrollees);

        if(selectedFaculty == null && limitItemsQty == null && offSetValueEnrollees == null ){
            //initial values for select
            selectedFaculty = "1";
            limitItemsQty = "5";
            offSetValueEnrollees = "0";
            session.setAttribute("selectValue", selectedFaculty);
            session.setAttribute("enrolleesLimit", limitItemsQty);
            session.setAttribute("enrolleesOffSet", 1);
            enrolleesList = new EnrolleeDao().getAll(new String[]{language, selectedFaculty, limitItemsQty, offSetValueEnrollees});
        }else if(selectedFaculty != null && limitItemsQty == null && offSetValueEnrollees == null){
            session.setAttribute("selectValue", selectedFaculty);
            limitItemsQty = String.valueOf(session.getAttribute("enrolleesLimit"));
            offSetValueEnrollees = String.valueOf(session.getAttribute("enrolleesOffSet"));
            enrolleesList = new EnrolleeDao().getAll(new String[]{language, selectedFaculty, limitItemsQty, offSetValueEnrollees});
        }else if(selectedFaculty == null && limitItemsQty != null && offSetValueEnrollees.equals("0")){
            selectedFaculty = String.valueOf(session.getAttribute("selectValue"));
            session.setAttribute("enrolleesLimit", limitItemsQty);
            session.setAttribute("enrolleesOffSet", 1);
            enrolleesList = new EnrolleeDao().getAll(new String[]{language, selectedFaculty, limitItemsQty, "0"});
        }else if(selectedFaculty == null && limitItemsQty != null && offSetValueEnrollees != null) {
            selectedFaculty = String.valueOf(session.getAttribute("selectValue"));
            session.setAttribute("enrolleesLimit", limitItemsQty);
            session.setAttribute("enrolleesOffSet", offSetValueEnrollees);
            enrolleesList = new EnrolleeDao().getAll(new String[]{language, selectedFaculty, limitItemsQty, offSetValueEnrollees});
        }
        List<Faculty> facultiesList = new FacultyDao().getAll(new String[]{language});

        if (enrolleesList == null || facultiesList == null) {
            errorMessage = "Could not get enrolles list";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        log.debug("Got enrollees List with size --> " + enrolleesList.size());

        int totalDbEnrList = new EnrolleeDao().getEnrolleesListSize(new String[]{language, selectedFaculty});
        session.setAttribute("totalDbEnrList", totalDbEnrList);

        session.setAttribute("enrolleesList", enrolleesList);
        log.trace("Set the session attribute: enrolleesList --> " + enrolleesList);

        session.setAttribute("facultiesList", facultiesList);
        log.trace("Set the session attribute: facultiesList --> " + facultiesList);
        forward = Path.PAGE__ENROLLEE_CONTROL_DASH;

        log.debug("EnrolleesControlDashCommand finished with --> " + forward);
        return forward;
    }
}
