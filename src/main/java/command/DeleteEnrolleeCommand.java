package command;

import dao.EnrolleeDao;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Delete Enrollee entity from the db
 */
public class DeleteEnrolleeCommand extends Command {
    private static final long serialVersionUID = -66393200497797768L;
    private static final Logger log = Logger.getLogger(DeleteEnrolleeCommand.class);

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        log.debug("DeleteEnrolleeCommand request starts");
        // obtain id to delete from the request
        String enrolleeId = request.getParameter("deleteEnrolleeId");
        log.debug("Request parameter: deleteEnrolleeId --> " + enrolleeId);

        int idToDelete = 0;
        // error handler
        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;
        try {
            idToDelete = Integer.parseInt(enrolleeId);
        } catch (NumberFormatException ex) {
            log.debug("Could not get id to delete from url " + ex);
            return forward;
        }
        if (idToDelete == 0 || idToDelete < 0) {
            errorMessage = "Not correct id to delete";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        boolean isDeleted = new EnrolleeDao().delete(idToDelete);

        if (!isDeleted) {
            errorMessage = "Could not delete enrollee from db";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        forward = Path.COMMAND__ENROLLEES_CONTROL_DASH;
        log.debug("DeleteEnrolleeCommand finished with --> " + forward);
        return forward;
    }
}