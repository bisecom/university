package command;

import dao.FacultyDao;
import model.Faculty;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Get change parameters of faculty from request and update the faculty in the db
 */
public class UpdateFacultyCommand extends Command {
    private static final long serialVersionUID = 7810540326227229881L;
    private static final Logger log = Logger.getLogger(UpdateFacultyCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("UpdateFacultyCommand starts");

        HttpSession session = request.getSession();
        Faculty facultyToUpdate = new Faculty();
        String facultyIdToUpdate = request.getParameter("updateFacultyValue");

        String facultyId = request.getParameter("facultyId");
        String budgetPlaces = request.getParameter("budgetQty");
        String totalPlaces = request.getParameter("totalQty");

        String facultyEngName = request.getParameter("englishName");
        String facultyUkrName = request.getParameter("ukrainianName");
        String facultyRusName = request.getParameter("russianName");

        // error handler
        String errorMessage;
        String forward = Path.PAGE__ERROR_PAGE;
        log.debug("UpdateFacultyCommand facultyIdToUpdate value --> " + facultyIdToUpdate);
        if (facultyId == null && budgetPlaces == null && totalPlaces == null && facultyEngName == null &&
                facultyUkrName == null && facultyRusName == null && facultyIdToUpdate == null) {
            errorMessage = "Required fields cannot be empty";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        if (facultyIdToUpdate != null && facultyId == null) {
            Faculty facultyToDisplay = new FacultyDao().getById(Integer.parseInt(facultyIdToUpdate), new String[]{});
            session.setAttribute("facultyToDisplay", facultyToDisplay);
            forward = Path.PAGE__ADMIN_FACULTIES_UPDATE;
            return forward;
        }

        facultyToUpdate.setId(Integer.parseInt(facultyId));
        facultyToUpdate.setBudgetPlacesQty(Integer.parseInt(budgetPlaces));
        facultyToUpdate.setTotalPlacesQty(Integer.parseInt(totalPlaces));
        facultyToUpdate.getNamesList().add(facultyEngName);
        facultyToUpdate.getNamesList().add(facultyUkrName);
        facultyToUpdate.getNamesList().add(facultyRusName);

        boolean isUpdated = new FacultyDao().update(facultyToUpdate, new String[]{});
        log.debug("Found in DB: faculty --> " + isUpdated);
        if (!isUpdated) {
            errorMessage = "Cannot update faculty!";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        forward = Path.COMMAND__FACULTIES_BOARD;
        log.debug("UpdateFacultyCommand finished with --> " + forward);
        return forward;
    }
}
