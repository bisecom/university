package command;

import dao.FacultyDao;
import model.Application;
import model.Faculty;
import org.apache.log4j.Logger;
import utils.ApplicationStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Retrieve complete list of applications from db and set their status to not proceed
 * and update changes at the db
 */
public class RollBackStatementCommand extends Command {
    private static final long serialVersionUID = -442707286517719279L;
    private static final Logger log = Logger.getLogger(RollBackStatementCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("CreateStatementCommand starts -->" + request);
        String forward = Path.PAGE__STATEMENT_CONTROL_DASH;

        HttpSession session = request.getSession();
        String language = (String) session.getAttribute("elanguage");

        List<Faculty> facultiesList = new FacultyDao().getAll(new String[]{language});
        List<List<Application>> applicationsList = new CreateStatementCommand().getApplsByFaculies(facultiesList, language);

        setInitialStatusToAppls(applicationsList);
        log.debug("List<List<Application>> size --> " + applicationsList.size());

        boolean isStatementComplete = new CreateStatementCommand().putApplsChangesToDb(applicationsList);
        if (!isStatementComplete) {
            return Path.PAGE__ERROR_PAGE;
        }
        log.debug("applicationsList to board size --> " + applicationsList.size());
        session.setAttribute("applicationsList", applicationsList.get(0));

        log.debug("RollBackStatementCommand finished with --> " + forward);
        return forward;
    }

    private void setInitialStatusToAppls(List<List<Application>> applicationsList){
        for (List<Application> aplls : applicationsList) {
            for (Application a : aplls) {
                a.setApplicationStatus(ApplicationStatus.NOT_PROCEED);
            }
        }
    }
}