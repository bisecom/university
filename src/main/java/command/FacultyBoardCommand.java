package command;

import dao.FacultyDao;
import model.Faculty;
import org.apache.log4j.Logger;
import utils.Helper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Fulfill getting faculty list from the db
 */
public class FacultyBoardCommand extends Command {
    private static final long serialVersionUID = -1438251542900998615L;
    private static final Logger log = Logger.getLogger(FacultyBoardCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("FacultyBoardCommand starts -->" + request);
        String forward = Path.PAGE__ADMIN_FACULTIES;

        HttpSession session = request.getSession();
        String language = (String) session.getAttribute("elanguage");

        //getting sorted filters
        String facultySort = request.getParameter("facultySort");
        log.debug("facultySort value -->" + facultySort);
        List<Faculty> facultiesList = new FacultyDao().getAll(new String[]{language});

        if(facultySort == null){
            Helper.getSortedList("nameAsc", facultiesList);
        }else {
            Helper.getSortedList(facultySort, facultiesList);
        }
        log.debug("FacultiesList to board size --> " +  facultiesList.size());
        session.setAttribute("facultiesList", facultiesList);

        log.debug("FacultyBoardCommand finished with --> " + forward);
        return forward;
    }
}