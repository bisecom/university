package command;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Settle language to session which was preferred by user
 */
public class LanguageSetCommand extends Command {
    private static final long serialVersionUID = -5176355668225804896L;
    private static final Logger log = Logger.getLogger(LanguageSetCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        log.debug("LanguageSetCommand starts");
        HttpSession session = request.getSession();

        String language = request.getParameter("language");
        log.debug("Request parameter: language --> " + language);

        String forward = Path.PAGE__INDEX;
        session.setAttribute("elanguage", language);

        log.debug("LanguageSetCommand finished with --> " + forward);
        return forward;
    }
}