package command;

import dao.EnrolleeDao;
import dao.FacultyDao;
import model.Enrollee;
import model.Faculty;
import org.apache.log4j.Logger;
import utils.Helper;
import utils.Role;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Fulfill logging logic. Retrieve login and password from the request,
 * compare password (SH-256) with available hash at db and check role.
 * Forward to different pages depending from user role.
 * @author Sergiy B.
 */
public class LoginCommand extends Command {
    private static final long serialVersionUID = 802733277637426263L;
    private static final Logger log = Logger.getLogger(LoginCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("LoginCommand starts");
        HttpSession session = request.getSession();

        // obtain login and password from the request
        String login = request.getParameter("login");
        log.trace("Request parameter: loging --> " + login);

        String password = request.getParameter("password");
        // error handler
        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;

        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
            errorMessage = "Login/password cannot be empty";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        password = Helper.getPasswordHash(password);
        Enrollee user = new EnrolleeDao().getByLogin(login);
        log.debug("Found in DB: user --> " + user.getEmail());
        if (user == null || !password.equals(user.getPassword())) {
            errorMessage = "Cannot find user with such login/password";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return Path.PAGE__INDEX;
        } else {
            Role userRole = user.getRole();
            log.trace("userRole --> " + userRole);
            String language = (String) session.getAttribute("elanguage");
            if (userRole == Role.ADMIN) {
                forward = Path.PAGE__ADMIN_PANEL;

                List<Faculty> facultiesList = new FacultyDao().getAll(new String[]{language});
                log.debug("FacultiesList to admin dash size --> " + facultiesList.size());
                session.setAttribute("facultiesList", facultiesList);
            }

            if (userRole == Role.USER)
                forward = Path.COMMAND__ENROLLEE_PANEL;
            log.debug("Enrollee id user.getId() --> " + user.getId());
            log.debug("Enrollee Language() --> " + language);
            Enrollee enrollee = new EnrolleeDao().getById(user.getId(), new String[]{language});
            session.setAttribute("user", enrollee);
            log.trace("Set the session attribute: user --> " + enrollee);

            session.setAttribute("userRole", userRole);
            log.trace("Set the session attribute: userRole --> " + userRole);
        }

        log.debug("LoginCommand finished with --> " + forward);
        return forward;
    }

}