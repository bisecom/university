package command;

import dao.FacultyDao;
import model.Faculty;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Locale;

/**
 * Fulfill deleting of faculty insert from db
 */
public class DeleteFacultyCommand extends Command{
    private static final long serialVersionUID = 461088540440463073L;
    private static final Logger log = Logger.getLogger(DeleteFacultyCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("DeleteFacultyCommand request starts");
        HttpSession session = request.getSession();
        String language = (String) session.getAttribute("elanguage");

        // obtain id to delete from the request
        String facultyId = request.getParameter("deleteFacultyValue");
        log.debug("Request parameter: deleteFacultyValue --> " + facultyId);

        int idToDelete = 0;
        // error handler
        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;
        try {
            idToDelete = Integer.parseInt(facultyId);
        } catch (NumberFormatException ex) {
            log.debug("Could not get id to delete from url " + ex);
            return forward;
        }
        if (idToDelete == 0 || idToDelete < 0) {
            errorMessage = "Faculty Id cannot be empty";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        boolean isDeleted = new FacultyDao().delete(idToDelete);

        if (!isDeleted) {
            errorMessage = "Faculty Id cannot be empty";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        List<Faculty> facultiesList = new FacultyDao().getAll(new String[]{language});
        log.debug("LoginCommand localeCode --> " +  language);
        session.setAttribute("facultiesList", facultiesList);

        forward = Path.PAGE__ADMIN_FACULTIES;
        log.debug("DeleteFacultyCommand finished with --> " + forward);
        return forward;
    }
}