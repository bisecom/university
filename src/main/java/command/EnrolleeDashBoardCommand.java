package command;

import dao.ApplicationDao;
import dao.EnrolleeDao;
import model.Application;
import model.Enrollee;
import org.apache.log4j.Logger;
import utils.ApplicationSearchChoice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Base64;
import java.util.List;

/**
 * Get Enrollee entity from db and forward it to jsp
 */
public class EnrolleeDashBoardCommand extends Command {
    private static final long serialVersionUID = -6770579332415989320L;
    private static final Logger log = Logger.getLogger(EnrolleeDashBoardCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("EnrolleeDashBoardCommand starts -->" + request);

        String forward = Path.COMMAND__INSERT_ENROLLEE_APPLICATION;
        HttpSession session = request.getSession();
        String language = (String)session.getAttribute("elanguage");

        Enrollee enrollee = (Enrollee) session.getAttribute("user");
        List<Application> enrolleApllList = new ApplicationDao().getAll(new String[]{language, String.valueOf(enrollee.getId()), ApplicationSearchChoice.ENROLLEE.toString()});
        if (enrolleApllList != null && enrolleApllList.size() > 0) {
            session.setAttribute("applicationsList", enrolleApllList);
            session.setAttribute("enrollee", enrollee);
            byte[] certificate = new EnrolleeDao().getCertificate(enrollee.getId());
            if (certificate.length > 0) {
                String certImage = Base64.getEncoder().encodeToString(certificate);
                session.setAttribute("certImage", certImage);
            }
            forward = Path.PAGE__ENROLLEE_DISPLAY_PROFILE;
            log.debug("EnrolleeDashBoardCommand finished with --> " + forward);
            return forward;
        }else{
            log.debug("EnrolleeDashBoardCommand finished with --> " + forward);
            return forward;
        }
    }
}