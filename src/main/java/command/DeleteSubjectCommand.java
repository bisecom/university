package command;

import dao.SubjectDao;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Delete Faculty insert from the db
 */
public class DeleteSubjectCommand extends Command {
    private static final long serialVersionUID = -2002035311970224201L;
    private static final Logger log = Logger.getLogger(DeleteSubjectCommand.class);

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        log.debug("DeleteSubjectCommand request starts");
        // obtain id to delete from the request
        String subjId = request.getParameter("deleteSubjectValue");
        log.debug("Request parameter: deleteSubjectValue --> " + subjId);

        int idToDelete = 0;
        // error handler
        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;
        try {
            idToDelete = Integer.parseInt(subjId);
        } catch (NumberFormatException ex) {
            log.debug("Could not get id to delete from url " + ex);
            return forward;
        }
        if (idToDelete == 0 || idToDelete < 0) {
            errorMessage = "Not correct id to delete";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        boolean isDeleted = new SubjectDao().delete(idToDelete);

        if (!isDeleted) {
            errorMessage = "Could not delete subject from db";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        forward = Path.COMMAND__LIST_SUBJECTS_BOARD;
        log.debug("DeleteSubjectCommand finished with --> " + forward);
        return forward;
    }
}