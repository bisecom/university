package command;

import dao.EnrolleeDao;
import model.Application;
import model.Enrollee;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Fulfill reception of all data from enrollee (excluding applications)
 * and insert it to the db
 */
public class RegistrationFinalCommand extends Command{
    private static final long serialVersionUID = -3274139322845425393L;
    private static final Logger log = Logger.getLogger(RegistrationFinalCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)  {
        log.debug("RegistrationFinalCommand starts -->" + request);
        HttpSession session = request.getSession();
        Enrollee e = (Enrollee) session.getAttribute("user");

            // obtain data from the request
            int user_id = e.getId();
            log.trace("Request parameter: id --> " + user_id);
            String email = e.getEmail();
            log.trace("Request parameter: email --> " + email);

            String firstName = request.getParameter("firstName");
            log.debug("Request parameter: firstName --> " + firstName);

            String fatherName = request.getParameter("fatherName");
            log.debug("Request parameter: fatherName --> " + fatherName);

            String secondName = request.getParameter("secondName");
            log.debug("Request parameter: secondName --> " + secondName);

            String city = request.getParameter("city");
            log.debug("Request parameter: city --> " + city);

            String state = request.getParameter("state");
            log.debug("Request parameter: state --> " + state);

            String schoolName = request.getParameter("schoolName");
            log.debug("Request parameter: schoolName --> " + schoolName);

        // error handler
        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;

        if (firstName == null || fatherName == null || secondName == null || city == null ||
                state == null || schoolName == null ||
                firstName.isEmpty() || fatherName.isEmpty() || secondName.isEmpty() ||
                city.isEmpty() || state.isEmpty() || schoolName.isEmpty()) {
            errorMessage = "Required fields cannot be empty";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        Enrollee user = new Enrollee(user_id, email, e.getRole(), firstName, fatherName, secondName,
                city, state, schoolName, "certificate".getBytes(), true, new ArrayList<Application>(), LocalDate.now());

        int enrolleeId  = new EnrolleeDao().create(user, new String[]{});
            if(enrolleeId == 0) {
                errorMessage = "Could not insert the Enrollee to the system!";
                log.error("errorMessage --> " + errorMessage);
                return Path.PAGE__ERROR_PAGE;
            }
        log.debug("RegistrationFinalCommand user DB id --> " + enrolleeId);
            session.setAttribute("user", user);
            log.trace("Set the session attribute: user --> " + user);
            forward = Path.COMMAND__INSERT_ENROLLEE_APPLICATION;

        log.debug("RegistrationFinalCommand finished with --> " + forward);
        return forward;
    }
}