package command;

import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

/**
 * Holder for all commands.<br/>
 * @author Sergiy B.
 */
public class CommandContainer {
    private static final Logger log = Logger.getLogger(CommandContainer.class);
    private static final Map<String, Command> commands = new TreeMap<String, Command>();

    static {
        // out of control
        commands.put("login", new LoginCommand());
        commands.put("loginChoice", new LoginChoiceCommand());
        commands.put("registrationChoice", new RegistrationChoiceCommand());
        commands.put("registrInitial", new RegistrationInitialCommand());
        commands.put("registerFinal", new RegistrationFinalCommand());
        commands.put("setLanguage", new LanguageSetCommand());
        commands.put("getFaculties", new GetFacultiesCommand());

        // common commands
        commands.put("logout", new LogoutCommand());

        // user commands
        commands.put("enrolleePanel", new EnrolleeDashBoardCommand());
        commands.put("insertEnrolleApplication", new InsertApplicationCommand());

        // admin commands
        commands.put("adminPanel", new AdminPanelCommand());
        commands.put("facultyDashBoard", new FacultyBoardCommand());
        commands.put("showNewFacultyForm", new ShowFacultyFormCommand());
        commands.put("createFaculty", new CreateFacultyCommand());
        commands.put("updateFaculty", new UpdateFacultyCommand());
        commands.put("deleteFaculty", new DeleteFacultyCommand());
        commands.put("subjectDashBoard", new SubjectBoardCommand());
        commands.put("createSubject", new CreateSubjectCommand());
        commands.put("deleteSubject", new DeleteSubjectCommand());
        commands.put("updateSubject", new UpdateSubjectCommand());
        commands.put("getEnrolleesControlDash", new EnrolleesControlDashCommand());
        commands.put("getEnrollee", new GetEnrolleeCommand());
        commands.put("deleteEnrollee", new DeleteEnrolleeCommand());
        commands.put("getStatementDash", new StatementDashBoardCommand());
        commands.put("createStatement", new CreateStatementCommand());
        commands.put("rollbackStatement", new RollBackStatementCommand());
        commands.put("getReportsDash", new RegistrationFinalCommand());

        log.debug("Command container was successfully initialized");
        log.trace("Number of commands --> " + commands.size());
    }

    /**
     * Returns command object with the given name.
     * @param commandName Name of the command.
     * @return Command object.
     */
    public static Command get(String commandName) {
        if(commandName.contains("?")){
            String commandToProceed = commandName.substring(0, commandName.indexOf("?"));
            log.debug("Command updated name --> " + commandToProceed);
            return commands.get(commandToProceed);
        }
        if (commandName == null || !commands.containsKey(commandName)) {
            log.trace("Command not found, name --> " + commandName);
            return commands.get("noCommand");
        }
        return commands.get(commandName);
    }

}