package utils;

public enum ApplicationStatus {
    NOT_PROCEED, NOT_APPROVED, BUDGET_APPROVED, CONTRACT_APPROVED
}
