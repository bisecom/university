package utils;

import model.Faculty;
import org.bouncycastle.util.encoders.Hex;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.List;

/**
 * Includes utils methods for the app
 */
public final class Helper {
    private Helper(){}

    public static String getPasswordHash(String password){
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
        return new String(Hex.encode(hash));
    }

    public static void getSortedList(String filter, List<Faculty> listToSort) {
        if (listToSort == null || listToSort.isEmpty())
            return;
        switch (filter) {
            case "nameAsc":
                listToSort.sort(Faculty.COMPARE_BY_NAME);
                break;
            case "nameDsc":
                listToSort.sort(Collections.reverseOrder(Faculty.COMPARE_BY_NAME));
                break;
            case "budgetAsc":
                listToSort.sort(Faculty.COMPARE_BY_BUDGET_PLACES);
                break;
            case "budgetDsc":
                listToSort.sort(Collections.reverseOrder(Faculty.COMPARE_BY_BUDGET_PLACES));
                break;
            case "totalAsc":
                listToSort.sort(Faculty.COMPARE_BY_TOTAL_PLACES);
                break;
            case "totalDsc":
                listToSort.sort(Collections.reverseOrder(Faculty.COMPARE_BY_TOTAL_PLACES));
                break;
            default:
                break;
        }
    }

}


