package db;

public final class Fields {

    //enrolly
    public static final String USER_ID = "user_logins_id";
    public static final String USER_LOGIN = "email";
    public static final String USER_PASSWORD = "password";
    public static final String USER_FIRST_NAME = "first_name";
    public static final String USER_FATHER_NAME = "father_name";
    public static final String USER_SECOND_NAME = "second_name";
    public static final String USER_CITY = "city";
    public static final String USER_STATE_NAME = "name";
    public static final String USER_SCHOOL = "school_name";
    public static final String USER_SCHOOL_CERT = "school_certificate";
    public static final String USER_IS_BLOCKED = "is_blocked";
    public static final String USER_ROLE = "user_roles_id";
    public static final String USER_ROLE_NAME = "name";
    public static final String USER_APPL_DATE = "appl_date";

    //application
    public static final String APPL_ID = "id";
    public static final String APPL_PRIORITY = "priority";
    public static final String APPL_IS_APPROVED = "is_approved";

    //faculty
    public static final String FACULTY_ID = "id";
    public static final String FACULTY_NAME = "name";
    public static final String FACULTY_BUDG_PLACES = "budget_places_qty";
    public static final String FACULTY_TOTAL_PLACES = "total_places_qty";

    //grade
    public static final String GRADE_ID = "id";
    public static final String GRADE_VALUE = "grade";

    //state
    public static final String STATE_ID = "id";

    //subject
    public static final String SUBJECT_ID = "id";
    public static final String SUBJECT_DURATION = "duration";
    public static final String SUBJECT_NAME = "name";

}
