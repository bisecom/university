package model;

import utils.ApplicationStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Application implements Serializable {
    private static final long serialVersionUID = -6038504292558516492L;
    private int id;
    private Enrollee enrollee;
    private Faculty faculty;
    private List<Grade> gradesList;
    private int priority;
    private ApplicationStatus applicationStatus;

    public Application(){
        this.gradesList = new ArrayList<>();
    }
    public Application(int id, Enrollee enrollee, Faculty faculty, List<Grade> grList, int priority, ApplicationStatus applicationStatus){
        this.id = id;
        this.enrollee = enrollee;
        this.faculty = faculty;
        this.priority = priority;
        this.applicationStatus = applicationStatus;
        this.gradesList = new ArrayList<>();
        listInitialize(grList);
    }

    public Application(Enrollee enrollee, Faculty faculty, List<Grade> grList, int priority, ApplicationStatus applicationStatus){
        this.enrollee = enrollee;
        this.faculty = faculty;
        this.priority = priority;
        this.applicationStatus = applicationStatus;
        this.gradesList = new ArrayList<>();
        listInitialize(grList);
    }
    private void listInitialize(List<Grade> grList) {
        gradesList.addAll(grList);
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public Enrollee getEnrollee() {
        return enrollee;
    }

    public void setEnrollee(Enrollee enrollee) {
        this.enrollee = enrollee;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public ApplicationStatus getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(ApplicationStatus applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public List<Grade> getGradesList() {
        return gradesList;
    }

    public static Comparator<Application> COMPARE_BY_AVERAGE_GRADE =
            (appl1, appl2) -> {
                int appl1AverGrade = 0;
                int appl2AverGrade = 0;
                for(Grade g : appl1.getGradesList()){
                    appl1AverGrade += g.getGrade();
                }
                if(appl1.getGradesList().size() > 0)
                appl1AverGrade = appl1AverGrade / appl1.getGradesList().size();

                for(Grade g : appl2.getGradesList()){
                    appl2AverGrade += g.getGrade();
                }
                if(appl2.getGradesList().size() > 0)
                appl2AverGrade = appl2AverGrade / appl2.getGradesList().size();
                return appl1AverGrade - appl2AverGrade;
            };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Application that = (Application) o;
        return id == that.id &&
                priority == that.priority &&
                Objects.equals(enrollee, that.enrollee) &&
                Objects.equals(faculty, that.faculty) &&
                Objects.equals(gradesList, that.gradesList) &&
                applicationStatus == that.applicationStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, enrollee, faculty, gradesList, priority, applicationStatus);
    }

    @Override
    public String toString() {
        return "Application{" +
                "id=" + id +
                ", enrollee=" + enrollee +
                ", faculty=" + faculty +
                ", gradesList=" + gradesList +
                ", priority=" + priority +
                ", applicationStatus=" + applicationStatus +
                '}';
    }
}
