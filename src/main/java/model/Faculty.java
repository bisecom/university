package model;

import java.io.Serializable;
import java.util.*;

public class Faculty implements Serializable {
    private static final long serialVersionUID = 3523165227703971485L;
    private int id;
    private List<String> namesList;
    private int budgetPlacesQty;
    private int totalPlacesQty;
    private List<Subject> subjectList;

    public Faculty(){
        subjectList = new ArrayList<>();
        namesList = new ArrayList<>();
    }
    public Faculty(int id){
        this.id = id;
    }
    public Faculty(int id, String[] names, int budgetPlacesQty, int totalPlacesQty){
        this.id = id;
        this.namesList = new ArrayList<>();
        this.budgetPlacesQty = budgetPlacesQty;
        this.totalPlacesQty = totalPlacesQty;
        this.subjectList = new ArrayList<>();
        listInitialize(names);
    }
    public Faculty(String[] names, int budgetPlacesQty, int totalPlacesQty){
        this.namesList = new ArrayList<>();
        this.budgetPlacesQty = budgetPlacesQty;
        this.totalPlacesQty = totalPlacesQty;
        this.subjectList = new ArrayList<>();
        listInitialize(names);
    }
    private void listInitialize(String[] localesNames) {
        namesList.addAll(Arrays.asList(localesNames));
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getNamesList() {
        return namesList;
    }

    public int getBudgetPlacesQty() {
        return budgetPlacesQty;
    }

    public void setBudgetPlacesQty(int budgetPlacesQty) {
        this.budgetPlacesQty = budgetPlacesQty;
    }

    public int getTotalPlacesQty() {
        return totalPlacesQty;
    }

    public void setTotalPlacesQty(int totalPlacesQty) {
        this.totalPlacesQty = totalPlacesQty;
    }

    public List<Subject> getSubjectList() {
        return subjectList;
    }

    public static Comparator<Faculty> COMPARE_BY_NAME = (one, other) -> one.getNamesList().get(0).compareTo(other.getNamesList().get(0));

    public static Comparator<Faculty> COMPARE_BY_BUDGET_PLACES = (one, other) -> one.getBudgetPlacesQty() - other.getBudgetPlacesQty();

    public static Comparator<Faculty> COMPARE_BY_TOTAL_PLACES = (one, other) -> one.getTotalPlacesQty() - other.getTotalPlacesQty();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Faculty faculty = (Faculty) o;
        return id == faculty.id &&
                budgetPlacesQty == faculty.budgetPlacesQty &&
                totalPlacesQty == faculty.totalPlacesQty &&
                namesList.equals(faculty.namesList) &&
                Objects.equals(subjectList, faculty.subjectList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, namesList, budgetPlacesQty, totalPlacesQty, subjectList);
    }

    @Override
    public String toString() {
        return "Faculty{" +
                "id=" + id +
                ", namesList=" + namesList +
                ", budgetPlacesQty=" + budgetPlacesQty +
                ", totalPlacesQty=" + totalPlacesQty +
                ", subjectList=" + subjectList +
                '}';
    }
}