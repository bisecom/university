package model;

import utils.Role;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Enrollee implements Serializable {
    private static final long serialVersionUID = 6851664337625923376L;
    private int id;
    private String email;
    private String password;
    private Role role;
    private String firstName;
    private String fatherName;
    private String secondName;
    private String city;
    private String state;
    private String schoolName;
    private byte[] schoolCertificate;
    private boolean isBlocked;
    private List<Application> applicationsList;
    private LocalDate applicationDate;

    public Enrollee(){
        applicationsList = new ArrayList<>();
    }

    public Enrollee(int id){
        this.id = id;
        applicationsList = new ArrayList<>();
    }

    public Enrollee(String email, String password){
        this.email = email;
        this.password = password;
        this.role = Role.USER;
        applicationsList = new ArrayList<>();
    }

    public Enrollee(int id, String email, String password){
        this.id = id;
        this.email = email;
        this.password = password;
        applicationsList = new ArrayList<>();
    }

    public Enrollee(int id, String email, Role role, String firstName, String fatherName,
                    String secondName, String city, String state, String schoolName, byte[] schoolCertificate,
                    boolean isBlocked, List<Application> aList, LocalDate applicationDate){
        this.id = id;
        this.email = email;
        this.role = role;
        this.firstName = firstName;
        this.fatherName = fatherName;
        this.secondName = secondName;
        this.city = city;
        this.state = state;
        this.schoolName = schoolName;
        this.schoolCertificate = schoolCertificate;
        this.isBlocked = isBlocked;
        this.applicationDate = applicationDate;
        this.applicationsList = new ArrayList<>();
        this.applicationsList.addAll(aList);
    }
    public Enrollee(String email, Role role, String firstName, String fatherName,
                    String secondName, String city, String state, String schoolName, byte[] schoolCertificate,
                    boolean isBlocked, List<Application> aList, LocalDate applicationDate){
        this.email = email;
        this.role = role;
        this.firstName = firstName;
        this.fatherName = fatherName;
        this.secondName = secondName;
        this.city = city;
        this.state = state;
        this.schoolName = schoolName;
        this.schoolCertificate = schoolCertificate;
        this.isBlocked = isBlocked;
        this.applicationDate = applicationDate;
        applicationsList = new ArrayList<>();
        this.applicationsList.addAll(aList);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public byte[] getSchoolCertificate() {
        return schoolCertificate;
    }

    public void setSchoolCertificate(byte[] schoolCertificate) {
        this.schoolCertificate = schoolCertificate;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public LocalDate getApplicationTime() {
        return applicationDate;
    }

    public List<Application> getApplicationsList() {
        return applicationsList;
    }

    public void setApplicationTime(LocalDate applicationTime) {
        this.applicationDate = applicationTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Enrollee enrollee = (Enrollee) o;
        return id == enrollee.id &&
                isBlocked == enrollee.isBlocked &&
                email.equals(enrollee.email) &&
                password.equals(enrollee.password) &&
                role == enrollee.role &&
                firstName.equals(enrollee.firstName) &&
                fatherName.equals(enrollee.fatherName) &&
                secondName.equals(enrollee.secondName) &&
                city.equals(enrollee.city) &&
                state.equals(enrollee.state) &&
                schoolName.equals(enrollee.schoolName) &&
                Arrays.equals(schoolCertificate, enrollee.schoolCertificate) &&
                Objects.equals(applicationsList, enrollee.applicationsList) &&
                applicationDate.equals(enrollee.applicationDate);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, email, password, role, firstName, fatherName, secondName, city, state, schoolName, isBlocked, applicationsList, applicationDate);
        result = 31 * result + Arrays.hashCode(schoolCertificate);
        return result;
    }

    @Override
    public String toString() {
        return "Enrollee{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", firstName='" + firstName + '\'' +
                ", fatherName='" + fatherName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", schoolName='" + schoolName + '\'' +
                ", schoolCertificate=" + Arrays.toString(schoolCertificate) +
                ", isBlocked=" + isBlocked +
                ", applicationsList=" + applicationsList +
                ", applicationDate=" + applicationDate +
                '}';
    }
}
