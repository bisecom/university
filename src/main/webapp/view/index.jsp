<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>
<c:set var="fList" value="${sessionScope['facultiesList']}"/>
<!DOCTYPE html>
<html>
<head>
    <title>Welcome page</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/styles.jspf" %>
    </style>
</head>
<body>
<!-- Login Header beginging -------------------->
<%@ include file="/view/jspf/loginHeader.jspf" %>
<!-- Login Header end -------------------------->
<br>
<div class="container indexMainCtr">
    <h2><fmt:message key="index.aboutUs"/></h2>
    <hr>
    <div id="about-us-text-id">
        <p class="indexText"><fmt:message key="index.sentence1"/></p>
        <p class="indexText"><fmt:message key="index.sentence2"/></p>
        <p class="indexText"><fmt:message key="index.sentence3"/></p>
    </div>

    <%--Hiden by default table--%>
    <table class="table table-hover" style="display:none" id="facultiesTable">
        <thead>
        <tr>
            <th>No</th>
            <th><fmt:message key="getEnrollee.Faculty"/>
                <a id="nameSort" href="" onclick="facultiesSort(this)">
                    <span class="glyphicon glyphicon-arrow-down"></span>
                </a></th>
            <th><fmt:message key="index.BudgetPlaces"/><a id="budgetSort" href="" onclick="facultiesSort(this)">
                <span class="glyphicon glyphicon-arrow-down"></span>
            </a></th>
            <th><fmt:message key="index.TotalPlaces"/><a id="totalSort" href="" onclick="facultiesSort(this)">
                <span class="glyphicon glyphicon-arrow-down"></span>
            </a></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${fList}" var="s" varStatus="loop">
            <tr>
                <td>${loop.index + 1}</td>
                <td>${s.getNamesList().get(0)}</td>
                <td class="td-to-align">${s.getBudgetPlacesQty()}</td>
                <td class="td-to-align">${s.getTotalPlacesQty()}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<!-- Footer ------------------------------------------>
<%@ include file="/view/jspf/indexFooter.jspf" %>
<!-- JavaScript functions ---------------------------->
<%@ include file="/view/js/javascript.jspf" %>

</body>
</html>
