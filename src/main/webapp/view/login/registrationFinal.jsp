<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>
<html>
<head>
    <title>Final Registration</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/styles.jspf" %>
    </style>
</head>
<body>
<!-- Header----------------------------------------------------------------------------------->
<%@ include file="/view/jspf/loginedHeader.jspf" %>
<!-- Body begining--------------------------------------------------------------------------->

<div class="container-fluid indexMainCtr">
    <h2><fmt:message key="registrationFinal.Title"/></h2>
    <hr>
    <div class="row">
        <div class="col-sm-12 col-sm-12-custom">
            <h3 id="table-header"><fmt:message key="registrationFinal.EnrolleeDetails"/></h3>

            <form method="POST" action="controller" class="was-validated">
                <input type="hidden" name="command" value="registerFinal"/>
                <div class="form-group">
                    <label for="firstName"><fmt:message key="enrollesControlDash.tableThFirstName"/></label>
                    <input type="text" class="form-control" id="firstName" placeholder="<fmt:message key="enrollesControlDash.tableThFirstName"/>" name="firstName"
                           required>
                    <div class="invalid-feedback">Please fill out first Name</div>
                </div>
                <div class="form-group">
                    <label for="fatherName"><fmt:message key="registrationFinal.FatherName"/></label>
                    <input type="text" class="form-control" id="fatherName" placeholder="<fmt:message key="registrationFinal.FatherNamePlch"/>"
                           name="fatherName" required>
                    <div class="invalid-feedback">Please fill out Father Name</div>
                </div>
                <div class="form-group">
                    <label for="secondName"><fmt:message key="enrollesControlDash.tableThSecondName"/></label>
                    <input type="text" class="form-control" id="secondName" placeholder="<fmt:message key="enrollesControlDash.tableThSecondName"/>"
                           name="secondName" required>
                    <div class="invalid-feedback">Please fill out Second Name</div>
                </div>
                <div class="form-group">
                    <label for="city"><fmt:message key="enrollesControlDash.tableThCity"/></label>
                    <input type="text" class="form-control" id="city" placeholder="<fmt:message key="enrollesControlDash.tableThCity"/>" name="city" required>
                    <div class="invalid-feedback">Please fill out City</div>
                </div>
                <div class="form-group">
                    <label for="state"><fmt:message key="enrollesControlDash.tableThState"/></label>
                    <input type="text" class="form-control" id="state" placeholder="<fmt:message key="enrollesControlDash.tableThState"/>" name="state" required>
                    <div class="invalid-feedback">Please fill out State</div>
                </div>
                <div class="form-group">
                    <label for="schoolName"><fmt:message key="registrationFinal.SchoolName"/></label>
                    <input type="text" class="form-control" id="schoolName" placeholder="<fmt:message key="registrationFinal.SchoolNamePlch"/>"
                           name="schoolName" required>
                    <div class="invalid-feedback">Please fill out School Name</div>
                </div>
                <div class="form-group form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="remember" required> <br><span><fmt:message key="registrationFinal.IAgree"/></span>
                        <div class="invalid-feedback"><fmt:message key="registrationFinal.CheckBox"/></div>
                    </label>
                </div>
                <button type="submit" class="btn btn-primary"><fmt:message key="registrationFinal.RegisterProfile"/></button>
            </form>

        </div>
    </div>
</div>

<!-- Footer ----------------------------------------->
<%@ include file="/view/jspf/footer.jspf" %>
</body>
</html>
