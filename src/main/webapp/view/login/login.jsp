<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>
<html>
<head>
    <title>User DashBoard</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/styles.jspf" %>
        <%@ include file="/view/css/modalStyle.jspf" %>
    </style>
</head>
<body>
<!-- Login Header beginging------------------------------------------------------------------->
<%@ include file="/view/jspf/loginHeader.jspf" %>

<!-- Body beginging--------------------------------------------------------------------------->
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-sm-12 title-div">
            <h2 class="text-xs-center"><fmt:message key="login.SignIn"/></h2>
            <hr>
        </div>
        <div class="col-sm-4 form-div">
            <form id="login_form" action="controller" method="post" class="was-validated" onsubmit="return loginFormValidation()">
                <input type="hidden" name="command" value="login"/>
                <div class="form-group">
                    <label for="login"><fmt:message key="login.Username"/></label>
                    <input type="text" class="form-control form-input" id="login" name="login" placeholder="<fmt:message key="login.EnterUsername"/>" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <div class="form-group">
                    <label for="password"><fmt:message key="login.Password"/></label>
                    <input type="password" class="form-control form-input" id="password" placeholder="<fmt:message key="login.EnterPassword"/>" name="password" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <br>
                <button type="submit" class="btn btn-primary" id="login-btn"><fmt:message key="index.login"/></button>
                <a class="link" href="${pageContext.request.contextPath}/controller?command=logout">
                    <button type="submit" class="btn btn-primary"><fmt:message key="adminDash.logout"/></button>
                </a>
            </form>
        </div>
    </div>
</div>
<!-- Modal ------------------------------------------>
<%@ include file="/view/jspf/modal.jspf" %>
<!-- Footer ----------------------------------------->
<%@ include file="/view/jspf/footer.jspf" %>
<!-- Footer ----------------------------------------->
<!-- JavaScript functions ---------------------------->
<%@ include file="/view/js/javascript.jspf" %>
</body>
</html>

