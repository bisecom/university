<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>
<html>
<head>
    <title>User DashBoard</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/styles.jspf" %>
        <%@ include file="/view/css/modalStyle.jspf" %>
    </style>
</head>
<body>
<!-- Login Header beginging------------------------------------------------------------------->
<%@ include file="/view/jspf/loginHeader.jspf" %>
<!-- Body beginging--------------------------------------------------------------------------->
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-sm-12 title-div">
            <h2 class="text-xs-center"><fmt:message key="registration.Title"/></h2>
            <hr>
        </div>
        <div class="col-sm-5 form-div">
            <form id="registration_form" action="controller" method="post" class="was-validated" onsubmit="return initialFormRegistValidation()">
                <input type="hidden" name="command" value="registrInitial"/>
                <div class="form-group">
                    <label for="email"><fmt:message key="login.Username"/></label>
                    <input type="text" class="form-control form-input" id="email" name="email" placeholder="<fmt:message key="login.EnterUsername"/>" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <div class="form-group">
                    <label for="password"><fmt:message key="login.Password"/></label>
                    <input type="password" class="form-control form-input" id="password" placeholder="<fmt:message key="login.EnterPassword"/>" name="password" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <div class="form-group">
                    <label for="confirm-password"><fmt:message key="registration.ConfirmPassword"/></label>
                    <input type="password" class="form-control form-input" id="confirm-password" placeholder="<fmt:message key="registration.ReConfirmPassword"/>" name="confirm-password" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <br>
                <button type="submit" class="btn btn-primary"><fmt:message key="registration.SignUp"/></button>
                <a class="link" href="${pageContext.request.contextPath}/view/index.jsp">
                    <button type="submit" class="btn btn-primary"><fmt:message key="adminDash.logout"/></button>
                </a>
            </form>
        </div>
    </div>
</div>
<!-- Modal ------------------------------------------>
<%@ include file="/view/jspf/modal.jspf" %>
<!-- JavaScript functions ---------------------------->
<%@ include file="/view/js/javascript.jspf" %>
<!-- Footer ----------------------------------------->
<%@ include file="/view/jspf/footer.jspf" %>

</body>
</html>
