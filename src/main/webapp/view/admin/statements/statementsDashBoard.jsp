<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>
<c:set var="applications" value="${sessionScope['applicationsList']}" />
<c:set var="faculties" value="${sessionScope['facultiesList']}" />
<html>
<head>
    <title>Statement Dashboard</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/statementDashCss.jspf" %>
    </style>
</head>
<body>
<!-- Header----------------------------------------------------------------------------------->
<%@ include file="/view/jspf/loginedHeader.jspf" %>
<!-- Body beginging--------------------------------------------------------------------------->

<div class="container-fluid indexMainCtr">
    <h2><fmt:message key="statementsDash.Title"/></h2>
    <hr>
    <div class="row">
        <div class="col-sm-3">
            <p style="font-weight: bold;"><fmt:message key="enrollesControlDash.navTitle"/></p>
            <ul class="nav nav-pills flex-column custom">
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/controller?command=createStatement"><fmt:message key="statementsDash.ProceedStatement"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/controller?command=rollbackStatement"><fmt:message key="statementsDash.RollBack"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/controller?command=getStatementDash"><fmt:message key="statementsDash.Mail"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/view/admin/adminDashBoard.jsp"><fmt:message key="facultiesDash.ReturnPill"/></a>
                </li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-9-custom">
            <h3 id="table-header"><fmt:message key="statementsDash.ListOfAppls"/></h3>
            <hr>
            <c:set var="selectedFacultyId" value="${sessionScope['selectValue']}" />
            <select class="browser-default custom-select" name="faculty" id="selectFacultyStm" onchange="selectRun()">
                <%--@elvariable id="subjectsList" type="java.util.List"--%>
                <c:forEach items="${faculties}" var="faculty">
                    <option value="${faculty.getId()}"
                            <c:if test="${faculty.getId() eq selectedFacultyId}">selected="selected"</c:if>
                    >
                            ${faculty.getNamesList().get(0)}
                    </option>
                </c:forEach>
            </select><br>
            <hr>

            <table class="table table-hover" id="tApplications">
                <thead>
                <tr>
                    <th>No</th>
                    <th><fmt:message key="enrollesControlDash.tableThSecondName"/></th>
                    <th><fmt:message key="enrollesControlDash.tableThFirstName"/></th>
                    <th><fmt:message key="getEnrollee.Priority"/></th>
                    <th><fmt:message key="getEnrollee.AverageGrade"/></th>
                    <th><fmt:message key="getEnrollee.Status"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${applications}" var="a" varStatus="loop">
                    <tr>
                    <td>${loop.index + 1}</td>
                    <td>${a.getEnrollee().getSecondName()}</td>
                    <td>${a.getEnrollee().getFirstName()}</td>
                    <td>${a.getPriority()}</td>

                    <c:set var="avaregeGrade" value="${0}"/>
                    <c:forEach var="g" items="${a.getGradesList()}">
                        <c:set var="avaregeGrade" value="${avaregeGrade + g.getGrade()}"/>
                    </c:forEach>
                    <c:set var="avaregeGrade" value="${avaregeGrade / a.getGradesList().size()}"/>
                    <td>${fn:substringBefore(avaregeGrade, '.')}</td>
                    <td>${a.getApplicationStatus()}</td>

                    <c:forEach items="${a.getGradesList()}" var="g" varStatus="loop">
                        <tr>
                            <th>${g.getSubject().getNameList().get(0)}</th>
                        </tr>
                        <tr>
                            <td>${g.getGrade()}</td>
                        </tr>
                    </c:forEach>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Footer ----------------------------------------->
<%@ include file="/view/jspf/footer.jspf" %>
<!-- JavaScript functions ---------------------------->
<%@ include file="/view/js/statementDashJs.jspf" %>
</body>
</html>
