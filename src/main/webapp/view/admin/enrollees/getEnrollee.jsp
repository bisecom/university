<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>

<c:set var="enrollee" value="${sessionScope['enrollee']}" />
<c:set var="applList" value="${sessionScope['applicationsList']}" />
<c:set var="certImage" value="${sessionScope['certImage']}" />
<html>
<head>
    <title>Enrollee Details</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/styles.jspf" %>
    </style>
</head>
<body>
<!-- Header----------------------------------------------------------------------------------->
<%@ include file="/view/jspf/loginedHeader.jspf" %>
<!-- Body beginging--------------------------------------------------------------------------->
<div class="container indexMainCtr">
    <div class="col-sm-12 col-sm-12-custom">
        <h2><fmt:message key="getEnrollee.title"/></h2>
        <hr>
        <h3><c:out value="${enrollee.getSecondName()+= ' '+= enrollee.getFirstName()}"/></h3>
        <h5><c:out value="${'Login: ' += enrollee.getEmail()}"/></h5>
        <h4><c:out value="${enrollee.isBlocked() == true ? 'Status: Under Checking' : 'Status: Details Approved'}"/></h4>
        <hr>
        <h3 id="table-header"><fmt:message key="getEnrollee.tableTitle"/></h3>
        <hr>
        <table class="table table-hover">
            <thead>
            <tr>
                <th><fmt:message key="getEnrollee.Priority"/></th>
                <th><fmt:message key="getEnrollee.Faculty"/></th>
                <th><fmt:message key="getEnrollee.AverageGrade"/></th>
                <th><fmt:message key="getEnrollee.Status"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${applList}" var="a" varStatus="loop">
            <tr>
                <td class="td-to-align">${a.getPriority()}</td>
                <td>${a.getFaculty().getNamesList().get(0)}</td>

                    <c:set var="avaregeGrade" value="${0}"/>
                <c:forEach var="g" items="${a.getGradesList()}">
                    <c:set var="avaregeGrade" value="${avaregeGrade + g.getGrade()}"/>
                </c:forEach>
                    <c:set var="avaregeGrade" value="${avaregeGrade / a.getGradesList().size()}"/>
                <td class="td-to-align">${fn:substringBefore(avaregeGrade, '.')}</td>

                <td class="td-to-align">${a.getApplicationStatus()}</td>
                <c:forEach items="${a.getGradesList()}" var="g" varStatus="loop">
            <tr>
            <thead><th>${g.getSubject().getNameList().get(0)}</th></thead>
            </tr>
            <tr>
                <td>${g.getGrade()}</td>
            </tr>
            </c:forEach>
            </tr>
            </c:forEach>

            </tbody>
        </table>
        <br/><hr>
        <p><b><fmt:message key="getEnrollee.ApplApprovement"/></b></p>
        <div class="form-check">
            <span><label class="form-check-label" for="unBlocked"><fmt:message key="getEnrollee.ApproveEnrollee"/></label>
                <input style="margin-left: 10px;" type="radio" class="form-check-input rgEnrollees" id="unBlocked" name="permission" value="0"/></span>

            <span style="margin-left: 30px;"><label class="form-check-label" for="isBlocked"><fmt:message key="getEnrollee.HoldEnrollee"/></label>
                <input style="margin-left: 10px;" type="radio" class="form-check-input rgEnrollees" id="isBlocked" name="permission" value="1"/></span>
        </div>
        <br>
        <form action="${pageContext.request.contextPath}/controller" method="POST" id="saveChangesForm">
            <input type="hidden" name="command" value="getEnrolleesControlDash"/>
            <input type="hidden" name="isBlockedEnrollee" id="updateStatusId" value="">
            <input class="btn btn-primary" type="submit" value="<fmt:message key="getEnrollee.SaveChanges"/>">
        </form>
        <button class="btn btn-primary" type="button" id="getCertificateBtn" onclick="getCertEnrolleeProfile(this);"><fmt:message key="getEnrollee.DisplayCertificate"/></button>
        <a href="${pageContext.request.contextPath}/view/admin/enrollees/enrolleesControlDashBoard.jsp">
            <button class="btn btn-primary" type="button" ><fmt:message key="enrollesControlDash.return"/></button></a><br>

        <hr>
        <div id="certDiv" style="display:none">
            <h1><fmt:message key="getEnrollee.CertCopy"/></h1>
            <img src="data:image/jpg;base64,${certImage}" width="600"/>
        </div>
    </div>
</div>
<!-- Footer ------------------------------------------>
<%@ include file="/view/jspf/footer.jspf" %>

<!-- JavaScript functions ---------------------------->
<%@ include file="/view/js/javascript.jspf" %>
</body>
</html>
