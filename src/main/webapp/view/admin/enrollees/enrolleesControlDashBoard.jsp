<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>

<c:set var="enrollees" value="${sessionScope['enrolleesList']}" />
<c:set var="faculties" value="${sessionScope['facultiesList']}" />
<c:set var="enrolleesLimit" value="${sessionScope['enrolleesLimit']}" />
<c:set var="enrolleesOffSet" value="${sessionScope['enrolleesOffSet']}" />
<c:set var="totalDbEnrList" value="${sessionScope['totalDbEnrList']}" />

<html>
<head>
    <title>Control of Enrollees by Admin</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/styles.jspf" %>
        <%@ include file="/view/css/modalStyle.jspf" %>
    </style>
</head>
<body>
<!-- Header----------------------------------------------------------------------------------->
<%@ include file="/view/jspf/loginedHeader.jspf" %>
<!-- Body beginging--------------------------------------------------------------------------->
<div class="container-fluid indexMainCtr">
    <h2><fmt:message key="enrollesControlDash.title"/></h2>
    <hr>
    <div class="row">
        <div class="col-sm-3">
            <p style="font-weight: bold;"><fmt:message key="enrollesControlDash.navTitle"/></p>
            <ul class="nav nav-pills flex-column custom">
                <li class="nav-item">
                    <form action="${pageContext.request.contextPath}/controller" method="POST" id="showEnrolleeForm">
                        <input type="hidden" name="command" value="getEnrollee"/>
                        <input type="hidden" name="selectedEnrolleeId" id="enrolleeId" value="">
                        <a class="nav-link" href="javascript:{}" onclick="displayEnrolly();"><fmt:message key="enrollesControlDash.dispSelected"/></a>
                    </form>
                </li>
                <li class="nav-item">
                    <form action="${pageContext.request.contextPath}/controller" method="POST" id="deleteEnrolleeForm">
                        <input type="hidden" name="command" value="deleteEnrollee"/>
                        <input type="hidden" name="deleteEnrolleeId" id="enrolleeToDeleteId" value="">
                        <a class="nav-link" href="javascript:{}" onclick="deleteEnrolly();"><fmt:message key="enrollesControlDash.removeSelected"/></a>
                    </form>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/view/admin/adminDashBoard.jsp"><fmt:message key="enrollesControlDash.return"/></a>
                </li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-9-custom">
            <h3 id="table-header"><fmt:message key="enrollesControlDash.tableTile"/></h3>
            <hr>
            <c:set var="selectedFacultyId" value="${sessionScope['selectValue']}" />
            <select class="browser-default custom-select" name="faculty" id="selectFaculty" onchange="selectRun()">
                <%--@elvariable id="subjectsList" type="java.util.List"--%>
                <c:forEach items="${faculties}" var="faculty">
                    <option value="${faculty.getId()}"
                        <%--@elvariable id="selectedSubjId" type="java"--%>
                            <c:if test="${faculty.getId() eq selectedFacultyId}">selected="selected"</c:if>
                    >
                            ${faculty.getNamesList().get(0)}
                    </option>
                </c:forEach>
            </select><br>
            <hr>
            <div id="pagination">
                <div id="listingTable"></div>
                <a href="javascript:prevPage()" id="btn_prev">Prev</a>
                <a href="javascript:nextPage()" id="btn_next">Next</a>
                page: <span id="page"></span>
                <select class="browser-default custom-select" id="paginationEnrl" onchange="getPage(this)">
                    <option value="2">2</option>
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                </select>
            </div>

            <table class="table table-hover" id="tEnrollees">
                <thead>
                <tr>
                    <th>No</th>
                    <th><fmt:message key="enrollesControlDash.tableThSecondName"/></th>
                    <th><fmt:message key="enrollesControlDash.tableThFirstName"/></th>
                    <th><fmt:message key="enrollesControlDash.tableThCity"/></th>
                    <th><fmt:message key="enrollesControlDash.tableThState"/></th>
                    <th><fmt:message key="enrollesControlDash.tableThStatus"/></th>
                    <th><fmt:message key="enrollesControlDash.tableThApplicationDate"/></th>
                    <th><fmt:message key="enrollesControlDash.tableThSelect"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${enrollees}" var="e" varStatus="loop">
                    <tr>
                        <td>${loop.index + 1}</td>
                        <td>${e.getSecondName()}</td>
                        <td>${e.getFirstName()}</td>
                        <td>${e.getCity()}</td>
                        <td>${e.getState()}</td>
                        <td class="td-to-align">${e.isBlocked() == true ? "Review Needed" : "Approved"}</td>
                        <td class="td-to-align">${e.getApplicationTime()}</td>
                        <td class="td-to-align">
                            <INPUT type="radio" class="rgEnrollees" name="rgEnrollees" value="${e.getId()}"/>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal ------------------------------------------>
<%@ include file="/view/jspf/modal.jspf" %>
<!-- Footer ----------------------------------------->
<%@ include file="/view/jspf/footer.jspf" %>

<div id="enrolleesLimit" style="display: none">${enrolleesLimit}</div>
<div id="enrolleesOffSet" style="display: none">${enrolleesOffSet}</div>
<div id="totalListSize" style="display: none">${totalDbEnrList}</div>

<!-- JavaScript functions ---------------------------->
<%@ include file="/view/js/enrolleeControlDashJs.jspf" %>
<%@ include file="/view/js/modal.jspf" %>

</body>
</html>
