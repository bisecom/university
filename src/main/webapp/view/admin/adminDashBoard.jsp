<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>
<c:set var="fList" value="${sessionScope['facultiesList']}"/>

<html>
<head>
    <title>Admin Dash Board</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/adminDashCss.jspf" %>
    </style>
</head>
<body>
<!-- Header----------------------------------------------------------------------------------->
<%@ include file="/view/jspf/loginedHeader.jspf" %>
<!-- Body beginging--------------------------------------------------------------------------->

<div class="container-fluid indexMainCtr">
    <h2><fmt:message key="adminDash.AdminDashBoard"/></h2>
    <hr>
    <div class="row">
        <div class="col-sm-3">
            <p style="font-weight: bold;"><fmt:message key="enrollesControlDash.navTitle"/></p>
            <ul class="nav nav-pills flex-column custom">
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/controller?command=subjectDashBoard"><fmt:message key="adminDash.SubjectsPill"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/controller?command=facultyDashBoard"><fmt:message key="adminDash.FacultiesPill"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/controller?command=getEnrolleesControlDash"><fmt:message key="adminDash.EnrolleesPill"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/controller?command=getStatementDash"><fmt:message key="adminDash.StatementPill"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/controller?command=getReportsDash"><fmt:message key="adminDash.ReportsPill"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/controller?command=logout"><fmt:message key="enrollesControlDash.return"/></a>
                </li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-9-custom">
            <h2 id="table-header"><fmt:message key="facultiesDash.OurFaculties"/></h2>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th><fmt:message key="adminDash.FacultiesPill"/> </th>
                    <th><fmt:message key="index.BudgetPlaces"/> </th>
                    <th><fmt:message key="index.TotalPlaces"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${fList}" var="f" varStatus="loop">
                    <tr>
                        <td>${loop.index + 1}</td>
                        <td>${f.getNamesList().get(0)}</td>
                        <td class="td-to-align">${f.getBudgetPlacesQty()}</td>
                        <td class="td-to-align">${f.getTotalPlacesQty()}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Footer ----------------------------------------->
<%@ include file="/view/jspf/footer.jspf" %>
<!-- JavaScript functions ---------------------------->
<%@ include file="/view/js/adminDashJs.jspf" %>
</body>
</html>
