<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>

<html>
<head>
    <title>Create Subject</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/createSubjectCss.jspf" %>
    </style>
</head>
<body>
<!-- Header----------------------------------------------------------------------------------->
<%@ include file="/view/jspf/loginedHeader.jspf" %>
<!-- Body beginging--------------------------------------------------------------------------->
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-sm-12 title-div">
            <h2 class="text-xs-center"><fmt:message key="createSubj.Title"/></h2>
            <hr>
        </div>
        <div class="col-sm-6 form-div">
            <form method="POST" action='${pageContext.request.contextPath}/controller?command=createSubject' id="subjectForm" class="was-validated">
                <div class="form-group">
                    <label for="englishName"><fmt:message key="updateSubj.SubjNameInEngl"/></label>
                    <input type="text" class="form-control form-input" id="englishName" placeholder="<fmt:message key="updateSubj.NameInEnglish"/>" name="englishName" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <div class="form-group">
                    <label for="ukrainianName"><fmt:message key="updateSubj.SubjNameInUkr"/></label>
                    <input type="text" class="form-control form-input" id="ukrainianName" placeholder="<fmt:message key="updateSubj.NameInUkrainian"/>" name="ukrainianName" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <div class="form-group">
                    <label for="russianName"><fmt:message key="updateSubj.SubjNameInRu"/></label>
                    <input type="text" class="form-control form-input" id="russianName" placeholder="<fmt:message key="updateSubj.NameInRussian"/>" name="russianName" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <div class="form-group">
                    <label for="courseDuration"><fmt:message key="updateSubj.CourseDuration"/></label>
                    <input type="text" class="form-control form-input" id="courseDuration" placeholder="<fmt:message key="updateSubj.CourseDurationPlcHlr"/>" name="courseDuration" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <br>
                <button type="submit" class="btn btn-primary"><fmt:message key="createSubj.Title"/></button>
                <a href="${pageContext.request.contextPath}/view/admin/subjects/subjectDashBoard.jsp">
                    <button type="button" class="btn btn-primary"><fmt:message key="createFaculty.CancelBtn"/></button></a>
            </form>
        </div>
    </div>
</div>

<!-- Footer ----------------------------------------->
<%@ include file="/view/jspf/footer.jspf" %>
</body>
</html>
