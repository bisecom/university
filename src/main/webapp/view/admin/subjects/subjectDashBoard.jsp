<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>
<c:set var="sList" value="${sessionScope['subjectsList']}"/>
<html>
<head>
    <title>Subjects DashBoard</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/subjectsDashBoardCss.jspf" %>
        <%@ include file="/view/css/modalStyle.jspf" %>
    </style>
</head>
<body>
<!-- Header----------------------------------------------------------------------------------->
<%@ include file="/view/jspf/loginedHeader.jspf" %>
<!-- Body beginging--------------------------------------------------------------------------->
<div class="container-fluid indexMainCtr">
    <h2><fmt:message key="subjectsDash.Title"/></h2>
    <hr>
    <div class="row">
        <div class="col-sm-3">
            <p style="font-weight: bold;"><fmt:message key="enrollesControlDash.navTitle"/></p>
            <ul class="nav nav-pills flex-column custom">
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/view/admin/subjects/createSubject.jsp"><fmt:message key="facultiesDash.CreatePill"/></a>
                </li>
                <li class="nav-item">
                    <form action="${pageContext.request.contextPath}/controller" method="POST" id="updateIdForm">
                        <input type="hidden" name="command" value="updateSubject"/>
                        <input type="hidden" name="updateSubjectValue" id="updateS" value="">
                        <a class="nav-link" href="javascript:{}" onclick="updateSubj();"><fmt:message key="facultiesDash.UpdatePill"/></a>
                    </form>
                </li>
                <li class="nav-item">
                    <form action="${pageContext.request.contextPath}/controller" method="POST" id="deleteIdForm">
                        <input type="hidden" name="command" value="deleteSubject"/>
                        <input type="hidden" name="deleteSubjectValue" id="deleteS" value="">
                        <a class="nav-link" href="javascript:{}" onclick="deleteSubj();"><fmt:message key="facultiesDash.DeletePill"/></a>
                    </form>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/view/admin/adminDashBoard.jsp"><fmt:message key="facultiesDash.ReturnPill"/></a>
                </li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-9-custom">
            <h2 id="table-header"><fmt:message key="subjectsDash.OurSubjects"/></h2>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>No</th>
                    <th><fmt:message key="createFaculty.SubjectName"/></th>
                    <th><fmt:message key="subjectsDash.CourseDuration"/></th>
                    <th><fmt:message key="enrollesControlDash.tableThSelect"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${sList}" var="s" varStatus="loop">
                    <tr>
                        <td>${loop.index + 1}</td>
                        <td>${s.getNameList().get(0)}</td>
                        <td class="td-to-align">${s.getCourseDuration()}</td>
                        <td class="td-to-align">
                            <INPUT type="radio" class="rgSubjects" name="rgSubjects" value="${s.getId()}"/>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal ------------------------------------------>
<%@ include file="/view/jspf/modal.jspf" %>
<!-- Footer ----------------------------------------->
<%@ include file="/view/jspf/footer.jspf" %>
<!-- JavaScript functions ---------------------------->
<%@ include file="/view/js/subjectsDashJs.jspf" %>
<%@ include file="/view/js/modal.jspf" %>
</body>
</html>

