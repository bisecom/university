<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>
<c:set var="fList" value="${sessionScope['facultiesList']}" />
<html>
<head>
    <title>Faculties DashBoard</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/styles.jspf" %>
        <%@ include file="/view/css/modalStyle.jspf" %>
    </style>
</head>
<body>
<!-- Header----------------------------------------------------------------------------------->
<%@ include file="/view/jspf/loginedHeader.jspf" %>
<!-- Body begining--------------------------------------------------------------------------->
<div class="container-fluid indexMainCtr">
    <h2><fmt:message key="facultiesDash.Title"/></h2>
    <hr>
    <div class="row">
        <div class="col-sm-3">
            <p style="font-weight: bold;"><fmt:message key="enrollesControlDash.navTitle"/></p>
            <ul class="nav nav-pills flex-column custom">
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/controller?command=showNewFacultyForm"><fmt:message key="facultiesDash.CreatePill"/></a>
                </li>
                <li class="nav-item">
                    <form action="${pageContext.request.contextPath}/controller" method="POST" id="updateIdForm">
                        <input type="hidden" name="command" value="updateFaculty"/>
                        <input type="hidden" name="updateFacultyValue" id="updateF" value="">
                        <a class="nav-link" href="javascript:{}" onclick="updateFaculty();"><fmt:message key="facultiesDash.UpdatePill"/></a>
                    </form>
                </li>
                <li class="nav-item">
                    <form action="${pageContext.request.contextPath}/controller" method="POST" id="deleteIdForm">
                        <input type="hidden" name="command" value="deleteFaculty"/>
                        <input type="hidden" name="deleteFacultyValue" id="deleteF" value="">
                        <a class="nav-link" href="javascript:{}" onclick="deleteFaculty();"><fmt:message key="facultiesDash.DeletePill"/></a>
                    </form>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/view/admin/adminDashBoard.jsp"><fmt:message key="facultiesDash.ReturnPill"/></a>
                </li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-9-custom">
            <h2 id="table-header"><fmt:message key="facultiesDash.OurFaculties"/></h2>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>No</th>
                    <th><fmt:message key="getEnrollee.Faculty"/><a id="nameSort" href="" onclick="facultiesSortDash(this)">
                        <span class="glyphicon glyphicon-arrow-down"></span>
                    </a></th>
                    <th><fmt:message key="createFaculty.BudgetPlacesPlcHolder"/><a id="budgetSort" href="" onclick="facultiesSortDash(this)">
                        <span class="glyphicon glyphicon-arrow-down"></span>
                    </a></th>
                    <th><fmt:message key="createFaculty.TotalPlacesPlcHolder"/><a id="totalSort" href="" onclick="facultiesSortDash(this)">
                        <span class="glyphicon glyphicon-arrow-down"></span>
                    </a></th>
                    <th><fmt:message key="enrollesControlDash.tableThSelect"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${fList}" var="f" varStatus="loop">
                    <tr>
                        <td>${loop.index + 1}</td>
                        <td>${f.getNamesList().get(0)}</td>
                        <td class="td-to-align">${f.getBudgetPlacesQty()}</td>
                        <td class="td-to-align">${f.getTotalPlacesQty()}</td>
                        <td>
                            <INPUT type="radio" class="radiogroup" name="radiogroup" value="${f.getId()}"/>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal ------------------------------------------>
<%@ include file="/view/jspf/modal.jspf" %>
<!-- Footer ----------------------------------------->
<%@ include file="/view/jspf/footer.jspf" %>

<!-- JavaScript functions ---------------------------->
<%@ include file="/view/js/facultiesDashJs.jspf" %>
<%@ include file="/view/js/modal.jspf" %>

</body>
</html>

