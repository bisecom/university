<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>

<html>
<head>
    <title>Create Faculty</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/createFacultyCss.jspf" %>
    </style>
</head>
<body>
<!-- Header----------------------------------------------------------------------------------->
<%@ include file="/view/jspf/loginedHeader.jspf" %>
<!-- Body beginging--------------------------------------------------------------------------->

<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-sm-12 title-div">
            <h2 class="text-xs-center"><fmt:message key="createFaculty.Title"/></h2>
            <hr>
        </div>
        <div class="col-sm-6 form-div">
            <form method="POST" action="controller" id="facultyForm" class="was-validated">
                <input type="hidden" name="command" value="createFaculty"/>
                <div class="form-group">
                    <label for="englishName"><fmt:message key="createFaculty.NameInEnglish"/></label>
                    <input type="text" class="form-control form-input" id="englishName" placeholder="<fmt:message key="createFaculty.PlaceHolderEngl"/>" name="englishName" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <div class="form-group">
                    <label for="ukrainianName"><fmt:message key="createFaculty.NameInUkrainian"/></label>
                    <input type="text" class="form-control form-input" id="ukrainianName" placeholder="<fmt:message key="createFaculty.PlaceHolderUkr"/>" name="ukrainianName" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <div class="form-group">
                    <label for="russianName"><fmt:message key="createFaculty.NameInRussian"/></label>
                    <input type="text" class="form-control form-input" id="russianName" placeholder="<fmt:message key="createFaculty.PlaceHolderRu"/>" name="russianName" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <div class="form-group">
                    <label for="budgetQty"><fmt:message key="createFaculty.BudgetPlaces"/></label>
                    <input type="text" class="form-control form-input" id="budgetQty" placeholder="<fmt:message key="createFaculty.BudgetPlacesPlcHolder"/>" name="budgetQty" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <div class="form-group">
                    <label for="totalQty"><fmt:message key="createFaculty.TotalPlaces"/></label>
                    <input type="text" class="form-control form-input" id="totalQty" placeholder="<fmt:message key="createFaculty.TotalPlacesPlcHolder"/>" name="totalQty" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <br>
                <hr>
                <div id="optionDiv">
                    <div id="dynamicInput[0]">
                        <label><fmt:message key="createFaculty.SubjectName"/></label>
                        <select class="browser-default custom-select" name="subject">
                            <%--@elvariable id="subjectsList" type="java.util.List"--%>
                            <c:forEach items="${subjectsList}" var="subject">
                                <option value="${subject.getId()}"
                                    <%--@elvariable id="selectedSubjId" type="java"--%>
                                        <c:if test="${subject.getId() eq selectedSubjId}">selected="selected"</c:if>
                                >
                                        ${subject.getNameList().get(0)}
                                </option>
                            </c:forEach>
                        </select>
                        <input class="btn btn-primary" type="button" value="+" onClick="addInput();">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary dynamic"><fmt:message key="createFaculty.CreateFaculty"/></button>
                <a href="${pageContext.request.contextPath}/view/admin/faculties/facultiesDashBoard.jsp">
                    <button type="button" class="btn btn-primary dynamic"><fmt:message key="createFaculty.CancelBtn"/></button></a>
            </form>
        </div>
    </div>
</div>
<!-- Footer ----------------------------------------->
<%@ include file="/view/jspf/footer.jspf" %>
<!-- JavaScript functions ---------------------------->
<%@ include file="/view/js/createFacultyJs.jspf" %>

</body>
</html>
