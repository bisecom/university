<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>

<html>
<head>
    <title>Update Faculty</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/updateFacultyCss.jspf" %>
    </style>
</head>
<body>

<c:set var="faculty" value="${sessionScope['facultyToDisplay']}" />
<!-- Header----------------------------------------------------------------------------------->
<%@ include file="/view/jspf/loginedHeader.jspf" %>
<!-- Body beginging--------------------------------------------------------------------------->

<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-sm-12 title-div">
            <h2 class="text-xs-center"><fmt:message key="updateFaculty.Title"/></h2>
            <hr>
        </div>
        <div class="col-sm-6 form-div">
            <form method="POST" action='${pageContext.request.contextPath}/controller?command=updateFaculty' id="facultyForm">
                <input type="hidden" name="facultyId" value="${faculty.getId()}"/>
                <div class="form-group">
                    <label for="englishName"><fmt:message key="createFaculty.NameInEnglish"/></label>
                    <input type="text" class="form-control form-input" id="englishName" placeholder="<fmt:message key="createFaculty.PlaceHolderEngl"/>" name="englishName" required
                           value="${faculty.getNamesList().get(0)}">
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <div class="form-group">
                    <label for="ukrainianName"><fmt:message key="createFaculty.NameInUkrainian"/></label>
                    <input type="text" class="form-control form-input" id="ukrainianName" placeholder="<fmt:message key="createFaculty.PlaceHolderUkr"/>" name="ukrainianName" required
                           value="${faculty.getNamesList().get(1)}">
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <div class="form-group">
                    <label for="russianName"><fmt:message key="createFaculty.NameInRussian"/></label>
                    <input type="text" class="form-control form-input" id="russianName" placeholder="<fmt:message key="createFaculty.PlaceHolderRu"/>" name="russianName" required
                           value="${faculty.getNamesList().get(2)}">
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <div class="form-group">
                    <label for="budgetQty"><fmt:message key="createFaculty.BudgetPlaces"/></label>
                    <input type="text" class="form-control form-input" id="budgetQty" placeholder="<fmt:message key="createFaculty.BudgetPlacesPlcHolder"/>" name="budgetQty" required
                           value="${faculty.getBudgetPlacesQty()}">
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <div class="form-group">
                    <label for="totalQty"><fmt:message key="createFaculty.TotalPlaces"/></label>
                    <input type="text" class="form-control form-input" id="totalQty" placeholder="<fmt:message key="createFaculty.TotalPlacesPlcHolder"/>" name="totalQty" required
                           value="${faculty.getTotalPlacesQty()}">
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
                <br>
                <button type="submit" class="btn btn-primary"><fmt:message key="updateFaculty.UpdateBtn"/></button>
                <a href="${pageContext.request.contextPath}/view/admin/faculties/facultiesDashBoard.jsp">
                    <button type="button" class="btn btn-primary"><fmt:message key="createFaculty.CancelBtn"/></button></a>
            </form>
        </div>
    </div>
</div>
<!-- Footer ----------------------------------------->
<%@ include file="/view/jspf/footer.jspf" %>

</body>
</html>
