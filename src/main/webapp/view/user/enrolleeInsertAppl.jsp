<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>

<html>
<head>
    <title>User DashBoard</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/styles.jspf" %>
    </style>
</head>
<body>
<!-- Header----------------------------------------------------------------------------------->
<%@ include file="/view/jspf/loginedHeader.jspf" %>
<!-- Body beginging--------------------------------------------------------------------------->
<c:set var="selectedFacultyId1" value="${sessionScope['priority1Subjects']}" />
<c:set var="selectedFacultyId2" value="${sessionScope['priority2Subjects']}" />
<c:set var="selectedFacultyId3" value="${sessionScope['priority3Subjects']}" />
<c:set var="facultiesList" value="${sessionScope['facultiesList']}" />
<div class="container indexMainCtr">
    <div class="col-sm-12 col-sm-12-custom">
        <h2><fmt:message key="enrolleeDash.Title"/></h2>
        <hr>
        <h3>${e.getSecondName() += " " += e.getFirstName()}</h3>
        <h4>${e.getCity()}</h4>
        <hr>
    </div>
    <div class="col-sm-6 col-sm-6-custom">
        <h3 id="table-header"><fmt:message key="enrolleeInsertAppl.TableTitle"/></h3>

        <div id="choice1" class="priorityAplication">
            <hr>
            <h4><fmt:message key="enrolleeInsertAppl.Priority1"/></h4>
            <label for="selectFaculty1"><fmt:message key="enrolleeInsertAppl.SelectFaculty"/></label>
            <select class="form-control" name="faculty" id="selectFaculty1" onchange="selectRun()">
                <c:forEach items="${facultiesList}" var="faculty">
                    <option value="${faculty.getId()}"

                            <c:if test="${faculty.getId() eq selectedFacultyId1}">selected="selected"</c:if>
                    >
                            ${faculty.getNamesList().get(0)}
                    </option>
                </c:forEach>
            </select>
            <br>
            <c:forEach items="${facultiesList.get(selectedFacultyId1 - 1).getSubjectList()}" var="s" varStatus="loop">
                <div class="form-group was-validated">
                    <label for="priority1subj${s.getId()}"><c:out value="${s.getNameList().get(0)}"/>:</label>
                    <input type="text" class="form-control prioritySubjects" id="priority1subj${s.getId()}" placeholder="<fmt:message key="enrolleeInsertAppl.YourZNOGrade"/>" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
            </c:forEach>
        </div>

        <div id="choice2" class="priorityAplication">
            <hr>
            <h4><fmt:message key="enrolleeInsertAppl.Priority2"/></h4>
            <label for="selectFaculty2"><fmt:message key="enrolleeInsertAppl.SelectFaculty"/></label>
            <select class="form-control" name="faculty" id="selectFaculty2" onchange="selectRun()">
                <c:forEach items="${facultiesList}" var="faculty">
                    <option value="${faculty.getId()}"

                            <c:if test="${faculty.getId() eq selectedFacultyId2}">selected="selected"</c:if>
                    >
                            ${faculty.getNamesList().get(0)}
                    </option>
                </c:forEach>
            </select>
            <br>
            <c:forEach items="${facultiesList.get(selectedFacultyId2 - 1).getSubjectList()}" var="s" varStatus="loop">
                <div class="form-group was-validated">
                    <label for="priority2subj${s.getId()}"><c:out value="${s.getNameList().get(0)}"/></label>
                    <input type="text" class="form-control prioritySubjects" id="priority2subj${s.getId()}" placeholder="<fmt:message key="enrolleeInsertAppl.YourZNOGrade"/>" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
            </c:forEach>
        </div>

        <div id="choice3" class="priorityAplication">
            <hr>
            <h4><fmt:message key="enrolleeInsertAppl.Priority3"/></h4>
            <label for="selectFaculty3"><fmt:message key="enrolleeInsertAppl.SelectFaculty"/></label>
            <select class="form-control" name="faculty" id="selectFaculty3" onchange="selectRun()">
                <c:forEach items="${facultiesList}" var="faculty">
                    <option value="${faculty.getId()}"

                            <c:if test="${faculty.getId() eq selectedFacultyId3}">selected="selected"</c:if>
                    >
                            ${faculty.getNamesList().get(0)}
                    </option>
                </c:forEach>
            </select>
            <br>
            <c:forEach items="${facultiesList.get(selectedFacultyId3 - 1).getSubjectList()}" var="s" varStatus="loop">
                <div class="form-group was-validated">
                    <label for="priority3subj${s.getId()}"><c:out value="${s.getNameList().get(0)}"/></label>
                    <input type="text" class="form-control prioritySubjects" id="priority3subj${s.getId()}" placeholder="<fmt:message key="enrolleeInsertAppl.YourZNOGrade"/>" required>
                    <div class="invalid-feedback"><fmt:message key="createFaculty.FormFail"/></div>
                </div>
            </c:forEach>
        </div>
        <hr>
        <form id="certUploadForm" name="certUploadForm" method="POST" action='${pageContext.request.contextPath}/controller?command=insertEnrolleApplication'
              enctype="multipart/form-data">
                <label class="btn btn-primary" for="certificateFile"><fmt:message key="enrolleeInsertAppl.CertificateImage"/></label>
                <input type="file" name="certificateFile" id="certificateFile" class="d-none"><br>
            <input type="submit" class="btn btn-primary" value="<fmt:message key="enrolleeInsertAppl.RegisterApplications"/>" onclick="submitForm();"/>
        </form>
    </div>
</div>

<!-- Footer ----------------------------------------->
<%@ include file="/view/jspf/footer.jspf" %>
<!-- JavaScript functions ---------------------------->
<%@ include file="/view/js/javascript.jspf" %>
</body>
</html>
