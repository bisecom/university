<%@ include file="/view/jspf/directives.jspf" %>
<%@ include file="/view/jspf/langSettings.jspf" %>

<c:set var="enrollee" value="${sessionScope['enrollee']}"/>
<c:set var="applList" value="${sessionScope['applicationsList']}"/>

<html>
<head>
    <title>Display Enrollee</title>
    <%@ include file="/view/jspf/headDirectives.jspf" %>
    <style>
        <%@ include file="/view/css/styles.jspf" %>
    </style>
</head>
<body>
<!-- Header----------------------------------------------------------------------------------->
<%@ include file="/view/jspf/loginedHeader.jspf" %>
<!-- Body beginging--------------------------------------------------------------------------->

<div class="container indexMainCtr">
    <div class="col-sm-12 col-sm-12-custom">
        <h2><fmt:message key="enrolleeDash.Title"/></h2>
        <hr>

        <h3 id="table-header"><fmt:message key="enrolleeDash.YourDetails"/></h3>
        <h3>${e.getSecondName() += " " += e.getFirstName()}</h3>
        <h4>${e.getCity()}</h4>
        <h4><c:out
                value="${enrollee.isBlocked() == true ? 'Enrollee Status: Under Checking' : 'Enrollee Status: Ddetails Approved'}"/></h4>

        <hr>
        <table class="table table-hover">
            <thead>
            <tr>
                <th><fmt:message key="getEnrollee.Priority"/></th>
                <th><fmt:message key="getEnrollee.Faculty"/></th>
                <th><fmt:message key="getEnrollee.AverageGrade"/></th>
                <th><fmt:message key="getEnrollee.Status"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${applList}" var="a" varStatus="loop">
            <tr>
                <td class="td-to-align">${a.getPriority()}</td>
                <td>${a.getFaculty().getNamesList().get(0)}</td>

                    <c:set var="avaregeGrade" value="${0}"/>
                <c:forEach var="g" items="${a.getGradesList()}">
                    <c:set var="avaregeGrade" value="${avaregeGrade + g.getGrade()}"/>
                </c:forEach>
                    <c:set var="avaregeGrade" value="${avaregeGrade / a.getGradesList().size()}"/>
                <td class="td-to-align">${fn:substringBefore(avaregeGrade, '.')}</td>

                <td class="td-to-align">${a.getApplicationStatus()}</td>
                <c:forEach items="${a.getGradesList()}" var="g" varStatus="loop">
            <tr>
            <thead>
            <th>${g.getSubject().getNameList().get(0)}</th>
            </thead>
            </tr>
            <tr>
                <td>${g.getGrade()}</td>

            </tr>
            </c:forEach>
            </tr>
            </c:forEach>

            </tbody>
        </table>
        <br/><br/>
        <button class="btn btn-primary" type="button" id="getCertbtnEnrlProfile" onclick="getCertEnrolleeProfile(this)"><fmt:message
                key="enrolleeDash.ShowCertificate"/></button>
        <a href="${pageContext.request.contextPath}/controller?command=logout">
            <button type="button" class="btn btn-primary"><fmt:message key="enrollesControlDash.return"/></button>
        </a><br>
        <hr>
        <div id="certDiv" style="display:none">
            <h1><fmt:message key="enrolleeDash.CertCopy"/></h1>
            <img src="data:image/jpg;base64,${certImage}" width="600"/>
        </div>
    </div>
</div>

<!-- Footer ----------------------------------------->
<%@ include file="/view/jspf/footer.jspf" %>

<!-- JavaScript functions ---------------------------->
<%@ include file="/view/js/javascript.jspf" %>

</body>
</html>